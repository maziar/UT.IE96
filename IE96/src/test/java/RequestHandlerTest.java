import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RequestHandlerTest {

    @Test
    public void testCalc_price() throws Exception {
        int adult_cnt, child_cnt, infant_cnt;
        adult_cnt = 2;
        child_cnt = 2;
        infant_cnt = 1;
        String prices = "1000 2000 3000"; //first:adult price, second: child price, third: infant price
        assertEquals(9000 , RequestHandler.calc_price(adult_cnt, child_cnt, infant_cnt, prices));
    }

    @Test
    public void testCalc_price2() throws Exception {
        int adult_cnt, child_cnt, infant_cnt;
        adult_cnt = -1;
        child_cnt = 0;
        infant_cnt = 1;
        String prices = "1000 2000 3000"; //first:adult price, second: child price, third: infant price
        assertEquals(-1 , RequestHandler.calc_price(adult_cnt, child_cnt, infant_cnt, prices));
    }

    @Test
    public void testCalc_price3() throws Exception {
        int adult_cnt, child_cnt, infant_cnt;
        adult_cnt = 0;
        child_cnt = 0;
        infant_cnt = 0;
        String prices = "1000 2000 3000"; //first:adult price, second: child price, third: infant price
        assertEquals(-1 , RequestHandler.calc_price(adult_cnt, child_cnt, infant_cnt, prices));
    }
}