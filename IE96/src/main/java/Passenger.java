/**
 * Created by monsieur maaz on 2/9/2017.
 */
public class Passenger {
    private String FirstName;
    private String SurName;
    private String NationalID;
    private Ticket ticket;

    public Passenger(String fn, String sn, String ni){
        FirstName = fn;
        SurName = sn;
        NationalID = ni;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getSurName() {
        return SurName;
    }

    public String getNationalID() {
        return NationalID;
    }

    String get_passenger_info() {
        return FirstName + " " + SurName + " " + NationalID + "\n";
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Ticket getTicket() {
        return ticket;
    }
}
