/**
 * Created by goodarzysepideh on 11/02/2017.
 */
public class Ticket {
    private String ticket_no ;

    public Ticket(String ticket_no) {
        this.ticket_no = ticket_no;
    }

    public String getTicket_no() {
        return ticket_no;
    }
}
