import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by goodarzysepideh on 12/02/2017.
 */
public class AirplaneTicket {

    public static void main(String[] args) throws IOException, ClassNotFoundException{
        int port_no = Integer.parseInt(args[0]) ;
        int apa_port_no = Integer.parseInt(args[2]);
        Server server =  new Server(port_no , args[1] , apa_port_no);
        server.run();
    }
}