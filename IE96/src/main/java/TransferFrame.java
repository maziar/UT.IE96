import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
/**
 * Created by monsieur maaz on 2/11/2017.
 */
public class TransferFrame extends JFrame {
    private JComboBox SearchFrom;
    private JComboBox SearchTo;
    private JComboBox SearchDate;
    private JTextField SearchAdNo;
    private JTextField SearchChNo;
    private JTextField SearchInfNo;

    private JButton Search;

    public TransferFrame() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String[] cities = new String[] {"THR", "MHD", "IFN"};
        String[] Dates = new String[] {"05Feb", "06Feb", "07Feb", "08Feb", "09Feb", "10Feb"};
        SearchFrom = new JComboBox<String>(cities);
        SearchTo = new JComboBox<String>(cities);
        SearchDate = new JComboBox<String>(Dates);
        SearchAdNo = new JTextField(3);
        SearchChNo = new JTextField(3);
        SearchInfNo = new JTextField(3);

        Search = new JButton("Search");
        Search.setPreferredSize(new Dimension(100, 20));
        Search.setAlignmentX(JButton.LEFT_ALIGNMENT);
        this.getContentPane().setBackground(Color.BLACK);
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        JPanel tempPanel = new JPanel(new FlowLayout());
        tempPanel.add(new JLabel("Source   "));
        tempPanel.add(SearchFrom);
        tempPanel.add(new JLabel("   destination   "));
        tempPanel.add(SearchTo);
        tempPanel.add(new JLabel("Enter Date   "));
        tempPanel.add(SearchDate);
        mainPanel.add(tempPanel);
        tempPanel = new JPanel(new FlowLayout());
        tempPanel.add(new JLabel("Adult Number   "));
        tempPanel.add(SearchAdNo);
        tempPanel.add(new JLabel("Child Number   "));
        tempPanel.add(SearchChNo);
        tempPanel.add(new JLabel("Infant Number  "));
        tempPanel.add(SearchInfNo);
        mainPanel.add(tempPanel);

        mainPanel.add(Search);

        Search.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String SearchFromInput = SearchFrom.getSelectedItem().toString();
                    String SearchToInput = SearchTo.getSelectedItem().toString();
                    String SearchDateInput = SearchDate.getSelectedItem().toString();
                    String SearchAdNoInput = SearchAdNo.getText();
                    String SearchChNoInput = SearchChNo.getText();
                    String SearchInfNoInput = SearchInfNo.getText();
                    if (SearchFromInput.equals("") || SearchToInput.equals("") ||
                            SearchDateInput.equals("") || SearchAdNoInput.equals("") ||
                            SearchChNoInput.equals("") || SearchInfNoInput.equals("")) {
                        JOptionPane.showMessageDialog(null, "One or more input are empty!", "Error", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        String query = "search " + SearchFromInput + " " + SearchToInput + " " +
                                SearchDateInput + " " + SearchAdNoInput + " " + SearchChNoInput + " " + SearchInfNoInput;

                        Socket socket = new Socket("localhost", 9091);

                        BufferedReader in = new BufferedReader(
                                new InputStreamReader(socket.getInputStream()));
                        PrintWriter out = new PrintWriter(new BufferedWriter(
                                new OutputStreamWriter(socket.getOutputStream())), true);

                        out.println(query);
                        String p_res;
                        String response = "";
                        do {
                            p_res = in.readLine();
                            response += (p_res + "\n");
                        /*System.out.println("[dbg] the partial response form server is: " + p_res);*/
                        } while (in.ready());
                        JOptionPane.showMessageDialog(null, response, "Search Result ", JOptionPane.INFORMATION_MESSAGE);
                        socket.close();
                    }

                } catch (IOException e1) {
                    System.out.println("Error!");
                    e1.printStackTrace();
                }

            }
        });
        this.getContentPane().add(mainPanel);
        this.pack();
    }
}