/**
 * Created by goodarzysepideh on 11/02/2017.
 */
public class SeatClass {
    private String seat_class ;
    private int adult_price ;
    private int child_price ;
    private int infant_price ;

    public SeatClass(String seat_class) {
        this.seat_class = seat_class;
    }

    public String getSeat_class() {
        return seat_class;
    }

    public void setChild_price(int child_price) {
        this.child_price = child_price;
    }

    public void setAdult_price(int adult_price) {
        this.adult_price = adult_price;
    }

    public void setInfant_price(int infant_price) {
        this.infant_price = infant_price;
    }
}
