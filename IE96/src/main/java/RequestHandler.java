import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by goodarzysepideh on 12/02/2017.
 */
public class RequestHandler {

    private Socket as_client ;
    private ArrayList<Reserve>treserves ;

    public RequestHandler(Socket as_client) {
        treserves = new ArrayList<Reserve>();
        this.as_client = as_client ;
    }

    public static int calc_price(int adult, int child, int infant, String res) throws ArrayIndexOutOfBoundsException{
        String[] splitted_res = res.split(" ");
        int adult_p = Integer.parseInt(splitted_res[0]);
        int child_p = Integer.parseInt(splitted_res[1]);
        int infant_p = Integer.parseInt(splitted_res[2]);
        if(adult < 0 || child < 0 || infant < 0 || (adult == 0 && child == 0 && infant == 0)){
            return -1 ;
        }
        return adult * adult_p + child * child_p + infant * infant_p;
    }

    public String process_res(ArrayList<String> response, String adult_cnt, String child_cnt, String infant_cnt) throws IOException {
        int adult = Integer.parseInt(adult_cnt);
        int child = Integer.parseInt(child_cnt);
        int infant = Integer.parseInt(infant_cnt);
        String res;
        String final_res = "" ;
        BufferedReader in = new BufferedReader(
                new InputStreamReader(as_client.getInputStream()));
        PrintWriter out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(as_client.getOutputStream())),true);
        for (int i = 0; i < response.size(); i+=2){
            String classes = "";
            String[] temp = response.get(i).split(" ");
            String airline_code ;
            String flight_no ;
            String orig_code ;
            String dest_code ;
            String departure_time ;
            String arrival_time ;
            String airplane_mod ;
            try {
                airline_code = temp[0];
                flight_no = temp[1];
                orig_code = temp[3];
                dest_code = temp[4];
                departure_time = temp[5].substring(0, 2) + ":" + temp[5].substring(2);
                arrival_time = temp[6].substring(0, 2) + ":" + temp[6].substring(2);
                airplane_mod = temp[7];
            }
            catch (ArrayIndexOutOfBoundsException e){
                return "" ;
            }
            String[] seat_classes = response.get(i+1).split(" ");
            System.out.println("[dbg] seat classes: " + seat_classes.length);
            for (int j = 0; j < seat_classes.length; j++){
                boolean is_avail = false;
                if (seat_classes[j].charAt(1) != 'A' && seat_classes[j].charAt(1) != 'C') {
                    int count = Character.getNumericValue(seat_classes[j].charAt(1));
                    if (count >= adult + child + infant){
                        System.out.println("[dbg] we are here!!");
                        is_avail = true;
                    }
                }
                else if (seat_classes[j].charAt(1) == 'A'){
                    System.out.println("[dbg] we are here!!");
                    is_avail = true;
                }

                if (is_avail == true){
                    String req_query = "PRICE" + " " + orig_code + " " + dest_code + " " + airline_code + " " + seat_classes[j].charAt(0);
                    System.out.println("requested query is : " + req_query);
                    try {
                        out.println(req_query);
                        res = in.readLine();
                    }
                    catch (IOException e){
                        return "" ;
                    }
                    System.out.println("price query result is : " + res);
                    try {
                        int price = calc_price(adult , child , infant , res);
                        if(price == -1){
                            return "bad request(wrong passengers number)!!!";
                        }
                        classes += ("\nClass: " + seat_classes[j].charAt(0) + "Price: "+ Integer.toString(price));
                    }
                    catch (ArrayIndexOutOfBoundsException e){
                        return "";
                    }
                }
            }
            if (! classes.equals("")){
                final_res += "Flight: " + airline_code + " " + flight_no + " Departure: " + departure_time + " Arrival: " + arrival_time +
                        " Airplane: " + airplane_mod + classes + "\n***\n" ;
            }
        }
        return final_res ;
    }

    public String search(String orig_code, String dest_code, String date, String adult_cnt, String child_cnt, String infant_cnt)throws IOException {
        ArrayList<String> response = new ArrayList<String>();
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(as_client.getInputStream()));
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new OutputStreamWriter(as_client.getOutputStream())), true);
            String req_query = "AV" + " " + orig_code + " " + dest_code + " " + date;
            System.out.println("[dbg] request query is: " + req_query);
            out.println(req_query);
            String p_res;
            do {
                p_res = in.readLine();
                response.add(p_res);
                System.out.println("[dbg] the partial response form server is: " + p_res);
                System.out.println("[dbg] size of partial res: " + p_res.length());
            } while (in.ready());
        }
        catch (IOException e){
            System.out.println("[dbg] error in the socket with the server");
            return "server is down , pls try again later , thanks for you being patient ";
        }
        System.out.println("[dbg] out from while ?");
        //ok up to here
        String final_res = process_res(response, adult_cnt, child_cnt, infant_cnt);
        System.out.println("[dbg] final response goes to client is: " + final_res);
        if(!final_res.equals("")){
            final_res =  final_res.substring(0,final_res.length()-4);
        }
        else{
            return "we don't have flights such as you have ordered or our server is down , pls try again later , thanks for you being patient ";
        }
        return final_res;
    }

    public String TemporalReserve(BufferedReader in, String orig_code, String dest_code, String date , String airline_code, String flight_no, String seat_class, String adult_cnt, String child_cnt, String infant_cnt) throws IOException {
        int adult = Integer.parseInt(adult_cnt);
        int child = Integer.parseInt(child_cnt);
        int infant = Integer.parseInt(infant_cnt);
        int flight_num = Integer.parseInt(flight_no);
        int total_pass = adult + child + infant;
        String response = "" ;
        System.out.println("[dbg] total passengers is: " + total_pass);
        Flight F = new Flight(orig_code,  dest_code, date, airline_code,  flight_num);
        SeatClass S = new SeatClass(seat_class);
        Reserve R = new Reserve(F, S, adult, child, infant);
        System.out.println("[dbg] the reserver info is : " + R.get_info());
        String query = "RES " + R.get_info();
        /*BufferedReader in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));*/
        BufferedReader in_from_server = new BufferedReader(
                new InputStreamReader(as_client.getInputStream()));
        PrintWriter out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(as_client.getOutputStream())),true);
        System.out.println("[dbg] out loop query is: " + query);
        out.flush();
        try {
            out.println(query);
            for (int i = 0; i < total_pass; i++) {
                out.flush();
                try {
                    String passenger_info = in.readLine();
                    System.out.println("[dbg] passengers info is : " + passenger_info);
                    String[] pass_info_params = passenger_info.split(" ");
                    System.out.println("[dbg] splitted passenger info: " + pass_info_params[0]);
                    System.out.println("[dbg] splitted passenger info: " + pass_info_params[1]);
                    System.out.println("[dbg] splitted passenger info: " + pass_info_params[2]);
                    R.add_passenger(pass_info_params[0], pass_info_params[1], pass_info_params[2]);
                    query = R.getPassengers().get(i).getFirstName() + " " + R.getPassengers().get(i).getSurName()
                            + " " + R.getPassengers().get(i).getNationalID();
                    System.out.println("[dbg] loop query: " + query);
                } catch (ArrayIndexOutOfBoundsException e) {
                    return "Our connection with you isn't working plz try later";
                }
                out.println(query);
                System.out.println("[dbg] adding passenger ...");
            }
            response = in_from_server.readLine();
            //query = "RES " + R.get_info() + R.get_list_of_passengers().substring(0, R.get_list_of_passengers().length()-1);
            //System.out.println("[dbg] the reserve querygo to server is : \n" + query);
            //out.println(query);
        }
        catch (IOException e){
            return "server is down , pls try again later , thanks for you being patient";
        }
        System.out.println("[dbg] the response of reserve from server is : " + response);
        String[] response_params = response.split(" ");
        try {
            int total_price = calc_price(adult, child, infant, response.substring(response.indexOf(" ") + 1, response.length()));
            if(total_price == -1){
                return "bad request(wrong passengers number)!!!";
            }
            R.setToken(response.substring(0, response.indexOf(" ")));
            R.setTotal_price(total_price);
            R.set_seat_class(Integer.parseInt(response_params[1]), Integer.parseInt(response_params[2]), Integer.parseInt(response_params[3]));
            treserves.add(R);
            return response.substring(0, response.indexOf(" ")) + " " + total_price;
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "server is down , pls try again later , thanks for you being patient";
        }
    }

    public String FinalReserve(String token) throws IOException{
        String finalized_res = "" ;
        for(int i = 0 ; i < treserves.size() ; i++){
            if(treserves.get(i).getToken().equals(token)){
                String orig_code = treserves.get(i).getFlight().getOrig_code();
                String dest_code = treserves.get(i).getFlight().getDest_code();
                String date  = treserves.get(i).getFlight().getDate();
                String airline_code  = treserves.get(i).getFlight().getAirline_code();
                String flight_no  = Integer.toString(treserves.get(i).getFlight().getFilght_no());
                String seat_class = treserves.get(i).getSeat_class().getSeat_class();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(as_client.getInputStream()));
                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(as_client.getOutputStream())),true);
                out.println("AV " + orig_code + " " + dest_code + " " + date);
                String p_res;
                String response = "";
                try {
                    do {
                        p_res = in.readLine();
                        response += p_res;
                        System.out.println("[dbg] the partial response form server is: " + p_res);
                        System.out.println("[dbg]size of partial res: " + p_res.length());
                    } while (in.ready());
                    String[] flights = response.split("\n");
                    for (int j = 0; j < flights.length; j += 2) {
                        String[] flights_params = flights[j].split(" ");
                        try {
                            if (flights_params[0].equals(airline_code) && flights_params[1].equals(flight_no)) {
                                String departure_time = flights_params[5];
                                String arrival_time = flights_params[6];
                                String airplane_model = flights_params[7];
                                treserves.get(i).set_flight_additional_info(departure_time, arrival_time, airplane_model);
                                out.println("FIN " + token);
                                ArrayList<Passenger> passengers = treserves.get(i).getPassengers();
                                String ref_code = in.readLine();
                                treserves.get(i).setRef_code(ref_code);
                                System.out.println("[dbg] the reference code is : " + ref_code);
                                String ticket_no = "";
                                for (int k = 0; k < passengers.size(); k++) {
                                    ticket_no = in.readLine();
                                    Ticket ticket = new Ticket(ticket_no);
                                    treserves.get(i).set_pass_ticktNo(k, ticket);
                                }
                                finalized_res = treserves.get(i).get_final_res_info();
                                break;
                            }
                        }
                        catch (ArrayIndexOutOfBoundsException e){
                            return "server is down , pls try again later , thanks for you being patient";
                        }
                        break;
                    }
                }
                catch (IOException e){
                    return "server is down , pls try again later , thanks for you being patient";
                }
            }
        }
        System.out.println("[dbg] the last response is : \n" + finalized_res);
        return  finalized_res ;
    }

    public String HandleRequest(String Req, BufferedReader sockbuf)throws IOException{
        String[] ReqParams = Req.split(" ");
        String res = "";
        try {
            if (ReqParams[0].equals("search")) {
                System.out.println("[dbg] ok we get the search!");
                //ok up to here
                res = search(ReqParams[1], ReqParams[2], ReqParams[3], ReqParams[4], ReqParams[5], ReqParams[6]);
            } else if (ReqParams[0].equals("reserve")) {
                System.out.println("[dbg] ok we get the Temporal Reserve!");
                res = TemporalReserve(sockbuf, ReqParams[1], ReqParams[2], ReqParams[3], ReqParams[4], ReqParams[5], ReqParams[6], ReqParams[7], ReqParams[8], ReqParams[9]);
            } else if (ReqParams[0].equals("finalize")) {
                System.out.println("[dbg] ok we get the Final Reserve!");
                res = FinalReserve(ReqParams[1]);
            }
            else if (Req.equals("quit")){
                return "quit";
            }
            else {
                return "bad request!";
            }
            return res;
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Our connection with you isn't working plz try later";
        }
    }

}
