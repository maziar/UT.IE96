import javax.sql.rowset.serial.SerialBlob;
import java.io.*;
import java.lang.reflect.Array;
import java.net.*;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.logging.SocketHandler;

/**
 * Created by monsieur maaz on 2/8/2017.
 */
public class Server {

    private int port_no ;
    private String apa_ip;
    private int apa_port_no ;
    private Socket as_client;

    public Server(int port_no, String apa_ip , int apa_port_no) throws IOException{
        this.port_no = port_no;
        this.apa_ip = apa_ip;
        this.apa_port_no = apa_port_no ;
        InetAddress apa_addr = InetAddress.getByName(apa_ip);
        while(true) {
            try {
                as_client = new Socket(apa_addr, apa_port_no);
                break;
            } catch (ConnectException e) {
                continue;
            }
        }
    }

    public void run() throws IOException, ClassNotFoundException {

        RequestHandler requestHandler = new RequestHandler(this.as_client);
        ServerSocket serverSocket = new ServerSocket(port_no);
        try {
            Socket socket = serverSocket.accept();
            try {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())), true);
                while (true) {
                    String response;
                    String req = in.readLine();
                    System.out.println("[dbg] client request is: " + req);
                    response = requestHandler.HandleRequest(req, in);
                    if (response.equals("quit"))
                        break;
                    out.println(response);
                    out.flush();
                }
            } finally {
                System.out.println("[dbg] client is closing");
                socket.close();
            }
        }
        finally {
            System.out.println("[dbg] server is closing");;
            serverSocket.close();
        }
    }

}
