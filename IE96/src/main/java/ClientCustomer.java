import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by monsieur maaz on 2/8/2017.
 */
public class ClientCustomer {

    public static void main(String[] args) throws IOException{
        run(Integer.parseInt(args[0]));
    }

    public static void run(int portno)throws IOException{
        InetAddress addr = InetAddress.getByName("localhost");
        Socket socket = new Socket(addr, portno);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
        PrintWriter out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream())),true);

        Scanner input = new Scanner(System.in);
        while (true) {
            String line = input.nextLine();
            if (line.equals("quit")) {
                out.println(line);
                socket.close();
                break;
            }
            String [] line_params = line.split(" ");
            if(line_params[0].equals("reserve")){
                System.out.println("[dbg] we are sending reserve");
                int  count =  Integer.parseInt(line_params[7]) + Integer.parseInt(line_params[8]) + Integer.parseInt(line_params[9]);
                //System.out.println("[dbg] count: " + count);
                for (int i = 0; i < count ; i++) {
                    //System.out.println("[dbg] i: " + i);
                    String passenger_info = "\n" + input.nextLine();
                    System.out.println("[dbg] passenger info: " + passenger_info);
                    line += passenger_info ;
                    //System.out.println("[dbg] line: " + line);
                }
            }
            System.out.println("[dbg] the request is : " + line);
            out.println(line);
            /*String response ;
            while ((response = in.readLine()) != null){
                System.out.println(response);
            }*/
            String p_res;
            String response = "";
            do {
                p_res = in.readLine();
                response += (p_res+"\n");
                /*System.out.println("[dbg] the partial response form server is: " + p_res);*/
            }while (in.ready());
            System.out.println(response);
        }
    }
}
