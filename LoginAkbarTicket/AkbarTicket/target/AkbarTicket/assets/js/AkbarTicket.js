(function(){
    var app = angular.module('AkbarTicket', ['ngRoute' ]);
    app.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : '/index.html',
                controller  : 'SearchController'
            })

            // route for the about page
            .when('/result', {
                templateUrl : '/result.html',
                controller  : 'ResultController'
            })

            // route for the contact page
            .when('/reserve', {
             templateUrl : '/Reserve.html',
             controller  : 'ReserveController'
            })

            .when('/tickets', {
                templateUrl : '/Tickets.html',
                controller  : 'ticketsController'
            })

            .when('/login', {
                templateUrl : '/login.html',
                controller  : 'loginController'
            });

    });
    app.service('sharedVars', function () {
        var property = [];

        return {
            getProperty: function () {
                return property;
            },
            setProperty: function(value) {
                property = value;
            }
        };
    });

    app.controller("SearchController", function($scope, $http, sharedVars, $location){
        localRef = this;
        /*this.validLogin = true;*/
        this.searchInfo={};
        this.searchReps = [];

        this.marshal = function(searchInfo) {
            return {
                orig_code: searchInfo.orig_code,
                dest_code: searchInfo.dest_code,
                date: searchInfo.date,
                adult_count: searchInfo.adult_count,
                child_count: searchInfo.child_count,
                infant_count: searchInfo.infant_count
            };
        };

        this.doSearch = function(searchInfo) {
            alert(this.searchInfo.child_count);
            alert("we are here!");
            localRef = this;
            $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/searchManagement/search' ,
                this.marshal(this.searchInfo)).then(
                function(response) {
                    localRef.searchReps = response.data ;
                    console.log(JSON.stringify(response.data));
                    sharedVars.setProperty(response.data);
                    $location.url('/result');
                }
            );
        };
    });

    app.controller("loginController", function($scope, $http, sharedVars, $location){
        localRef = this;
        /*this.validLogin = true;*/
        this.userInfo={};
        this.loginReps = [];

        this.marshal = function(userInfo) {
            return {
                username: userInfo.username,
                password: userInfo.password
            };
        };

        this.doLogin = function(userInfo){
            alert("we are here!");
            localRef = this;
            $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/loginManagement/login' ,
                this.marshal(this.userInfo)).then(
                function(response) {
                    console.log(response.data);
                    if(response.data == 0){
                        alert("نام کاربری یا رمز عبور اشتباه هست. لطفا دوباره تلاش کنید!")
                    }
                    else{
                        alert("ورود شما به سیستم موفقیت آمیز  بود!");
                        $location.url('/');
                    }
                }
            );
        };
    });

    app.controller("ResultController", function($scope, $http, sharedVars, $location){
        localRef = this;
        $scope.angFilterSort = "";
        $scope.angFilterAirline = "";
        $scope.angFilterSeatClass = "";
        $scope.searchResults = sharedVars.getProperty();
        
        /*console.log("size of json obj is" + $scope.searchResults.length);
        console.log("info is" + $scope.searchResults[0].orig_code);
        console.log("info is" + $scope.searchResults[0].dest_code);
        console.log("info is" + $scope.searchResults[0]["flight"]["flight_no"]);*/

        this.marshal = function(resultInfo) {
            return {
                airline_code: resultInfo.airline_code,
                flight_no:resultInfo.flight_no,
                date:resultInfo.date,
                orig_code:resultInfo.orig_code,
                dest_code:resultInfo.dest_code,
                departure_time:resultInfo.departure_time,
                arrival_time:resultInfo.arrival_time,
                airplane_model:resultInfo.airplane_model,
                total_prices :resultInfo.total_prices,
                seatClass :resultInfo.seatClass,
                adult_cnt:resultInfo.adult_cnt,
                child_cnt:resultInfo.child_cnt,
                inf_cnt:resultInfo.inf_cnt
            };
        };

        this.doReserve = function(index){
            alert("click ok to continue reserving!")
            localRef = this;
            console.log(index);
            console.log(JSON.stringify($scope.searchResults[index]));
            $scope.reserveInfo = {};
            $scope.reserveInfo.airline_code =  $scope.searchResults[index].flight.airline_code ;
            $scope.reserveInfo.flight_number = $scope.searchResults[index].flight.flight_no ;
            $scope.reserveInfo.date = $scope.searchResults[index].flight.date ;
            $scope.reserveInfo.orig_code = $scope.searchResults[index].flight.orig_code ;
            $scope.reserveInfo.dest_code = $scope.searchResults[index].flight.dest_code ;
            $scope.reserveInfo.departure_time = $scope.searchResults[index].flight.departure_time ;
            $scope.reserveInfo.arival_time = $scope.searchResults[index].flight.arrival_time ;
            $scope.reserveInfo.airplane_model = $scope.searchResults[index].flight.airplane_model ;
            $scope.reserveInfo.seat_class = $scope.searchResults[index].seatClass.seat_class ;
            $scope.reserveInfo.adult_count = $scope.searchResults[index].adult_cnt ;
            $scope.reserveInfo.adult_price = $scope.searchResults[index].seatClass.adult_price ;
            $scope.reserveInfo.child_count = $scope.searchResults[index].child_cnt ;
            $scope.reserveInfo.child_price = $scope.searchResults[index].seatClass.child_price ;
            $scope.reserveInfo.infant_count = $scope.searchResults[index].inf_cnt ;
            $scope.reserveInfo.infant_price = $scope.searchResults[index].seatClass.infant_price ;
            $scope.reserveInfo.total_price = $scope.searchResults[index].total_price ;
            $scope.reserveInfo.num_of_seats = $scope.searchResults[index].seatClass.count ;
            $scope.reserveInfo.converted = {} ;
            $scope.reserveInfo.converted.airline_code = $scope.searchResults[index].airline_code ;
            $scope.reserveInfo.converted.orig_city = $scope.searchResults[index].orig_code ;
            $scope.reserveInfo.converted.dest_city = $scope.searchResults[index].dest_code ;
            $scope.reserveInfo.converted.airplane_model = $scope.searchResults[index].airplane_model ;
            $scope.reserveInfo.converted.seat_class = $scope.searchResults[index].seatClass.seat_class;
            $scope.reserveInfo.converted.date = $scope.searchResults[index].flight.date ;
            $scope.reserveInfo.converted.departure_time = $scope.searchResults[index].flight.departure_time ;
            $scope.reserveInfo.converted.arrival_time = $scope.searchResults[index].flight.arrival_time ;
            $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/resultManagement/result' ,
                $scope.reserveInfo).then(
                function(response) {
                    console.log(response.data.airline_code);
                    console.log(response.data.airline_code == null);
                    if(response.data.airline_code == null)
                    {
                        alert("برای رزرو بلیط باید وارد سیستم شده باشید");
                        $location.url('/login');
                    }
                    else {
                        sharedVars.setProperty(response.data);
                        $location.url('/reserve');
                    }
                }
            );
        };

    });

    app.controller("ReserveController" , function($scope, $http , sharedVars, $location){
        localRef = this ;
        $scope.resultRep = sharedVars.getProperty();
        console.log(JSON.stringify($scope.resultRep));/*{
            "airline_code": "IR",
            "flight_number": 452,
            "date": "05Feb",
            "orig_code": "THR",
            "dest_code": "MHD",
            "departure_time": "1740",
            "arival_time": "1850",
            "airplane_model": "M80",
            "seat_class": "Y",
            "adult_count": 1,
            "adult_price": 3000,
            "child_count": 0,
            "child_price": 2000,
            "infant_count": 0,
            "infant_price": 1000,
            "total_price": 3000,
            "num_of_seats": 8,
            "converted": {
              "airline_code": "ایران ایر",
              "orig_city": "تهران",
              "dest_city": "مشهد",
              "airplane_model": "بوئینگ MD83",
              "seat_class": "کلاس اقتصادی",
              "date": "05Feb",
              "departure_time": "17:40",
              "arrival_time": "18:50"
            }
        };*/
        $scope.reserveInfo = {} ;
        $scope.reserveInfo.airline_code = $scope.resultRep.airline_code;
        $scope.reserveInfo.flight_no = $scope.resultRep.flight_number;
        $scope.reserveInfo.date = $scope.resultRep.date;
        $scope.reserveInfo.orig_code = $scope.resultRep.orig_code;
        $scope.reserveInfo.dest_code = $scope.resultRep.dest_code;
        $scope.reserveInfo.departure_time = $scope.resultRep.departure_time;
        $scope.reserveInfo.arival_time = $scope.resultRep.arival_time;
        $scope.reserveInfo.airplane_model = $scope.resultRep.airplane_model;
        $scope.reserveInfo.seat_class = $scope.resultRep.seat_class;
        $scope.reserveInfo.adult_count = $scope.resultRep.adult_count;
        $scope.reserveInfo.adult_price = $scope.resultRep.adult_price;
        $scope.reserveInfo.child_count = $scope.resultRep.child_count;
        $scope.reserveInfo.child_price = $scope.resultRep.child_price;
        $scope.reserveInfo.infant_count = $scope.resultRep.infant_count;
        $scope.reserveInfo.infant_price = $scope.resultRep.infant_price;
        $scope.reserveInfo.total_price = $scope.resultRep.total_price;
        $scope.reserveInfo.num_of_seats = $scope.resultRep.num_of_seats;
        $scope.reserveInfo.passengers = [] ;

        console.log(JSON.stringify($scope.resultRep));

        this.marshal = function(reserveInfo) {
            return {
                airline_code : reserveInfo.airline_code ,
                flight_no : reserveInfo.flight_no ,
                date : reserveInfo.date ,
                orig_code : reserveInfo.orig_code ,
                dest_code : reserveInfo.dest_code ,
                departure_time : reserveInfo.departure_time,
                arival_time : reserveInfo.arival_time ,
                airplane_model : reserveInfo.airplane_model ,
                seat_class : reserveInfo.seat_class ,
                adult_count : reserveInfo.adult_count ,
                adult_price : reserveInfo.adult_price ,
                child_count : reserveInfo.child_count ,
                child_price : reserveInfo.child_price ,
                infant_count : reserveInfo.infant_count ,
                infant_price : reserveInfo.infant_price ,
                total_price : reserveInfo.total_price ,
                num_of_seats : reserveInfo.num_of_seats ,
                passengers : reserveInfo.passengers ,
                token : $scope.resultRep.token
            };
        };

        this.doReserve = function(reserveInfo) {
            console.log(JSON.stringify(this.marshal($scope.reserveInfo)));
            $http.post('http://localhost:8080/AkbarTicket/AkbarTicket/reserveManagement/reserve' ,
                this.marshal($scope.reserveInfo)).then(
                function(response) {
                    console.log(JSON.stringify(response.data));
                    if(response.data.reserve == null)
                    {
                        alert("برای رزرو بلیط باید وارد سیستم شده باشید");
                        $location.url('/login');
                    }
                    else{
                        sharedVars.setProperty(response.data);
                        $location.url('/tickets');
                    }
                }
            );
        };

        $scope.add_passenger = function(mode) {
            if(mode == 1) {
                $scope.reserveInfo.adult_count++;
            }
            else if(mode == 2){
                $scope.reserveInfo.child_count++;
            }
            else if(mode == 3){
                $scope.reserveInfo.infant_count++;
            }
        };

    });

    app.controller("ticketsController", function($scope, $http , sharedVars, $location){
        $scope.reserveRep = sharedVars.getProperty();/*{
            "reserve": {
                "token": "cab5f36f-c536-33f4-6477-03efadaf9ac6",
                "_list_of_passengers": "aaa aaaaaa 1111111111\naaaaaa aaaaaa 1111111111\naaaaaa aaaaaa 1111111111\naaaaaa aaaaaa 1111111111\n",
                "_final_res_info": "aaa aaaaaa  34692d74-77bb-5779-5065-88a7c75ed639 2ae1297a-f112-7fe3-0c28-c0c3493929a3 THR MHD IR 452 Y 1740 1850 M80\naaaaaa aaaaaa  34692d74-77bb-5779-5065-88a7c75ed639 e94596d4-d535-3c27-a365-4cfaef99d62e THR MHD IR 452 Y 1740 1850 M80\naaaaaa aaaaaa  34692d74-77bb-5779-5065-88a7c75ed639 0400f67c-cb07-58ce-b090-181a9c401e76 THR MHD IR 452 Y 1740 1850 M80\naaaaaa aaaaaa  34692d74-77bb-5779-5065-88a7c75ed639 fff80145-f08a-8bcd-e301-37c0960f519a THR MHD IR 452 Y 1740 1850 M80",
                "child_cnt": 1,
                "ref_code": "34692d74-77bb-5779-5065-88a7c75ed639",
                "_info": "THR MHD 05Feb IR 452 Y 2 1 1",
                "total_price": 9000,
                "passengers": [
                    {
                        "FirstName": "aaa",
                        "SurName": "aaaaaa",
                        "NationalID": "1111111111",
                        "sex": "MR",
                        "ticket": {
                            "ticket_no": "2ae1297a-f112-7fe3-0c28-c0c3493929a3"
                        },
                        "surName": "aaaaaa",
                        "firstName": "aaa",
                        "nationalID": "1111111111"
                    }
                ],
                "infant_cnt": 1,
                "adult_cnt": 2,
                "flight": {
                    "date": "05Feb",
                    "orig_code": "THR",
                    "dest_code": "MHD",
                    "airplane_model": "M80",
                    "departure_time": "1740",
                    "airline_code": "IR",
                    "_filght_info": "THR MHD 05Feb IR 452",
                    "arrival_time": "1850",
                    "filght_no": 452
                },
                "seat_class": {
                    "count": 0,
                    "infant_price": 1000,
                    "adult_price": 3000,
                    "child_price": 2000,
                    "seat_class": "Y"
                }
            },
            "converted": {
                "date": "05Feb",
                "airplane_model": "بوئینگ MD83",
                "departure_time": "17:40",
                "airline_code": "ایران ایر",
                "seat_class": "کلاس اقتصادی",
                "arrival_time": "18:50",
                "orig_city": "تهران",
                "dest_city": "مشهد"
            }
        };*/
    });

    app.directive('nameDirective', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attr, mCtrl) {
                function myValidation(value) {
                    if (value.length > 2 && /^[a-zA-Z]+$/.test(value)) {
                        mCtrl.$setValidity('name', true);
                    } else {
                        mCtrl.$setValidity('name', false);
                    }
                    return value;
                }
                mCtrl.$parsers.push(myValidation);
            }
        };
    });

    app.directive('nidDirective', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attr, mCtrl) {
                function myValidation(value) {
                    if (value.length == 10) {
                        mCtrl.$setValidity('name', true);
                    } else {
                        mCtrl.$setValidity('name', false);
                    }
                    return value;
                }
                mCtrl.$parsers.push(myValidation);
            }
        };
    });
})();
