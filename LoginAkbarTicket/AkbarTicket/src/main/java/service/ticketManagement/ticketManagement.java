package service.ticketManagement;

import domain.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.log4j.Logger ;

/**
 * Created by goodarzysepideh on 27/04/2017.
 */
@Path("/ticketManagement")
public class ticketManagement {
    private static final Logger logger = Logger.getLogger(ticketManagement.class);

    @Context
    private HttpServletRequest request;

    @GET
    @Path("/getTickets")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Ticket> getTickets() throws IOException, ClassNotFoundException {
        ArrayList<Ticket> result = new ArrayList<Ticket>();
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        String accessLevel = (String) session.getAttribute("accessLevel");
        if (username == null) {
            logger.info("UnAthorized access");
            return result ;
        }
        Akbarticket Akbar = new Akbarticket("178.62.207.47", 8081);
        result = Akbar.getTickets(username , accessLevel);
        return result ;
    }

    @GET
    @Path("/getTickets/id={id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Ticket> getTicketById(@PathParam("id") int ticketId) throws IOException, ClassNotFoundException {
        ArrayList<Ticket> result = new ArrayList<Ticket>();
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        String accessLevel = (String) session.getAttribute("accessLevel");
        if (username == null) {
            logger.info("UnAthorized access");
            return result ;
        }
        Akbarticket Akbar = new Akbarticket("178.62.207.47", 8081);
        result = Akbar.getTicketById(username , accessLevel , ticketId);
        return result ;
    }
}
