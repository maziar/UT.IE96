package service.reserveManagement;

import domain.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import org.apache.log4j.Logger ;

/**
 * Created by goodarzysepideh on 27/04/2017.
 */
@Path("/reserveManagement")
public class reserveManagement {
    private static final Logger logger = Logger.getLogger(reserveManagement.class);

    @Context
    private HttpServletRequest request;

    @POST
    @Path("/reserve")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public VOFinResRep reserve(VOTempResReq VOTRQ) throws IOException, ClassNotFoundException {
        VOFinResRep VOFRP = new VOFinResRep();
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        String accessLevel = (String) session.getAttribute("accessLevel");
        if (username == null || accessLevel.equals("1") || !VOTRQ.token.equals((String) session.getAttribute("token"))) {
            logger.info("UnAthorized access");
            return VOFRP;
        }
        Akbarticket Akbar = new Akbarticket("178.62.207.47", 8081);
        convertor con = new convertor();
        ReserveClass RC = Akbar.reserve(VOTRQ , username);
        VOConverted voc = con.convert(VOTRQ.airline_code , VOTRQ.orig_code , VOTRQ.dest_code , VOTRQ.airplane_model ,
                VOTRQ.seat_class , VOTRQ.date , VOTRQ.departure_time , VOTRQ.arival_time );
        VOFRP.reserve = RC ;
        VOFRP.converted = voc ;
        return VOFRP ;
    }
}
