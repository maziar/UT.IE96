package service.resultManagement;

import domain.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import org.apache.log4j.Logger ;

/**
 * Created by goodarzysepideh on 27/04/2017.
 */
@Path("/resultManagement")
public class resultManagement {
    private static final Logger logger = Logger.getLogger(resultManagement.class);

    @Context
    private HttpServletRequest request;

    @POST
    @Path("/result")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public VOResultRep result(VOResultReq VORR) throws IOException, ClassNotFoundException {
        VOResultRep VOORRP = new VOResultRep();
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        String accessLevel = (String) session.getAttribute("accessLevel");
        SessionIdentifierGenerator sig = new SessionIdentifierGenerator();
        session.setAttribute("token", sig.nextSessionId());
        if (username == null || accessLevel.equals("1")) {
            logger.info("UnAthorized access");
            return VOORRP;
        }
        Akbarticket Akbar = new Akbarticket("178.62.207.47", 8081);
        VOORRP = Akbar.temp_reserve(VORR);
        VOORRP.token = (String) session.getAttribute("token");
        return VOORRP ;
    }
}
