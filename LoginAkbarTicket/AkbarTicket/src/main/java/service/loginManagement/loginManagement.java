package service.loginManagement;

import domain.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Created by goodarzysepideh on 27/04/2017.
 */
@Path("/loginManagement")
public class loginManagement {

    @Context
    private HttpServletRequest request;

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public int login(VOUser VOU) throws IOException, ClassNotFoundException {
        Akbarticket Akbar = new Akbarticket("178.62.207.47", 8081);
        int result = Akbar.login(VOU);
        if(result != 0) {
            HttpSession session = request.getSession();
            session.setAttribute("username", VOU.username);
            session.setAttribute("accessLevel", Integer.toString(result));
        }
        return result ;
    }
}
