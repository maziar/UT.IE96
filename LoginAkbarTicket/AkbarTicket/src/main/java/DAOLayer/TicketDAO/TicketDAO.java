package DAOLayer.TicketDAO;

import DAOLayer.DBConnection;
import domain.Ticket;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Mehrad on 5/23/2017.
 */
public class TicketDAO {
    public ArrayList<Ticket> getTickets(String username , String accessLevel){
        ArrayList<Ticket> result = new ArrayList<Ticket>();
        try {
            System.out.println(accessLevel);
            DBConnection con = new DBConnection();
            String q = "";
            if(accessLevel.equals("1")){
                q = "select ticket_no from passengers";
            }
            else if(accessLevel.equals("2")){
                q = "select ticket_no from passengers where username = '"+ username+"'";
            }
            ResultSet rs = con.execute_select(q);
            while(rs.next()){
                Ticket t = new Ticket(rs.getString("ticket_no"));
                result.add(t);
            }
        } catch (SQLException e) {
            System.out.println("Cannot connect the database! ..... " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<Ticket> getTicketById(String username , String accessLevel , int ticketID){
        ArrayList<Ticket> result = new ArrayList<Ticket>();
        try {
            System.out.println(accessLevel);
            DBConnection con = new DBConnection();
            String q = "";
            if(accessLevel.equals("1")){
                q = "select ticket_no from passengers where id = '" + ticketID + "'";
            }
            else if(accessLevel.equals("2")){
                q = "select ticket_no from passengers where username = '"+ username+"' and id = '"+ ticketID +"'";
            }
            ResultSet rs = con.execute_select(q);
            while(rs.next()){
                Ticket t = new Ticket(rs.getString("ticket_no"));
                result.add(t);
            }
            if(result.size() == 0){
                Ticket t = new Ticket("بلیط با این شماره وجود ندارد یا اینکه در دسترس شما نیست!");
                result.add(t);
            }
        } catch (SQLException e) {
            System.out.println("Cannot connect the database! ..... " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
