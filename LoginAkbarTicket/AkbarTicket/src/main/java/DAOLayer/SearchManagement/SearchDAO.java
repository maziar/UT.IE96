package DAOLayer.SearchManagement;

import DAOLayer.DBConnection;
import domain.*;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by monsieur maaz on 5/8/2017.
 */
public class SearchDAO {
    public VOSearchResponses search(VOSearchReq VOSRQ){
        VOSearchResponses VOP = new VOSearchResponses();
        int orig_id  = 0 , dest_id = 0 ;
        try {
            DBConnection con = new DBConnection();
            String q1 = "select ID from CITY where city_code = '" + VOSRQ.orig_code + "'";
            String q2 = "select ID from CITY where city_code = '" + VOSRQ.dest_code + "'";
            ResultSet rs = con.execute_select(q1);
            rs.next();
            orig_id = rs.getInt("id");
            rs = con.execute_select(q2);
            rs.next();
            dest_id = rs.getInt("id");
            String q3 = "select * from flight where orig_city_id = " + Integer.toString(orig_id) +
                        " and dest_city_id = " + Integer.toString(dest_id) + " and date = '" + VOSRQ.date + "'";
            rs = con.execute_select(q3);
            ArrayList<Integer> flight_IDs = new ArrayList<Integer>() ;
            while(rs.next()){
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                if( timestamp.getTime() - rs.getTimestamp("last_update_time").getTime()  > 30*60*1000) {
                    return VOP ;
                }
                flight_IDs.add(rs.getInt("id"));
            }
            ArrayList<VOSearchRep> SearchResponse = new ArrayList<VOSearchRep>();
            for(int i = 0 ; i < flight_IDs.size() ; i++){
                Flight flight ;
                ArrayList<Integer> total_prices = new ArrayList<Integer>();
                ArrayList<SeatClass> seatClasses = new ArrayList<SeatClass>();
                String q5 = "select * from flight where id = " + flight_IDs.get(i);
                ResultSet rs2 = con.execute_select(q5);
                rs2.next();
                String q6 = "select * from airline where id = " + rs2.getInt("airline_id");
                String q7 = "select * from airplane where id = " + rs2.getInt("airplane_id");
                ResultSet rs3 = con.execute_select(q6);
                ResultSet rs4 = con.execute_select(q7);
                rs3.next();
                rs4.next();
                String airline_code = rs3.getString("airline_code");
                String airplane_model = rs4.getString("airplane_model");
                flight = new Flight(VOSRQ.orig_code , VOSRQ.dest_code , VOSRQ.date ,
                        airline_code , rs2.getInt("flight_number"));
                flight.setDeparture_time(rs2.getString("departure_time").toString());
                flight.setArrival_time(rs2.getString("arrival_time").toString());
                flight.setAirplane_model(airplane_model);
                int num_of_passengers = Integer.parseInt(VOSRQ.adult_count) + Integer.parseInt(VOSRQ.child_count) + Integer.parseInt(VOSRQ.infant_cout) ;
                String q4 = "select * from flight_seatclass where flight_id = " + flight_IDs.get(i) + " and (empty_count = 9 or empty_count > " + num_of_passengers + ")";
                rs = con.execute_select(q4);
                while(rs.next()){
                    String q8 = "select * from seatclass where id = " + rs.getInt("seatclass_id");
                    ResultSet rs5 = con.execute_select(q8);
                    rs5.next();
                    SeatClass seat_class_temp = new SeatClass(rs5.getString("seatclass_code"));
                    seat_class_temp.setAdult_price(rs.getInt("adult_price"));
                    seat_class_temp.setChild_price(rs.getInt("child_price"));
                    seat_class_temp.setInfant_price(rs.getInt("infant_price"));
                    seat_class_temp.setCount(rs.getInt("empty_count"));
                    int total_price = Integer.parseInt(VOSRQ.adult_count) * rs.getInt("adult_price") +
                            Integer.parseInt(VOSRQ.child_count) * rs.getInt("child_price") +
                            Integer.parseInt(VOSRQ.infant_cout) * rs.getInt("infant_price") ;
                    total_prices.add(total_price);
                    seatClasses.add(seat_class_temp);
                }
                if(total_prices.size() != 0){
                    VOSearchRep VOSRP = new VOSearchRep(flight , total_prices , seatClasses , Integer.parseInt(VOSRQ.adult_count) , Integer.parseInt(VOSRQ.child_count)
                            , Integer.parseInt(VOSRQ.infant_cout));
                    SearchResponse.add(VOSRP);
                }
            }
            VOP.SearchResponse = SearchResponse ;
        } catch (SQLException e) {
            System.out.println("Cannot connect the database! ..... " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return VOP ;
    }

    public void update (VOSearchResponses VOP) {
        DBConnection con ;
        for(int i = 0 ; i < VOP.SearchResponse.size() ; i++){
            try {
                con = new DBConnection();
                String q1 = "select * from city where city_code = '" + VOP.SearchResponse.get(i).flight.getOrig_code() + "'" ;
                String q2 = "select * from city where city_code = '" + VOP.SearchResponse.get(i).flight.getDest_code() + "'" ;
                String q3 = "select * from airline where airline_code = '" + VOP.SearchResponse.get(i).flight.getAirline_code() + "'" ;
                String q4 = "select * from airplane where airplane_model = '" + VOP.SearchResponse.get(i).flight.getAirplane_model() + "'" ;
                ResultSet r1 = con.execute_select(q1);
                ResultSet r2 = con.execute_select(q2);
                ResultSet r3 = con.execute_select(q3);
                ResultSet r4 = con.execute_select(q4);
                r1.next();
                r2.next();
                r3.next();
                r4.next();
                int orig_city_id = r1.getInt("id");
                int dest_city_id = r2.getInt("id");
                int airline_id = r3.getInt("id");
                int airplane_id = r4.getInt("id");
                String q = "select id from flight where date = '" + VOP.SearchResponse.get(i).flight.getDate() +
                        "' and arrival_time = '" + VOP.SearchResponse.get(i).flight.getArrival_time() + "' and" +
                        " departure_time = '" + VOP.SearchResponse.get(i).flight.getDeparture_time() + "' and" +
                        " flight_number = " + VOP.SearchResponse.get(i).flight.getFlight_no() + " and" +
                        " orig_city_id = " + orig_city_id + " and airline_id = " + airline_id + " and" +
                        " airplane_id = " + airplane_id + " and dest_city_id = " + dest_city_id ;
                ResultSet rs = con.execute_select(q);
                int flight_id =  0 ;
                if(!rs.next()) {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    String q5 = "insert into flight (last_update_time , date , arrival_time ," +
                            "departure_time , flight_number , orig_city_id , airline_id , airplane_id , " +
                            "dest_city_id) values ( '" + timestamp + "' , '" + VOP.SearchResponse.get(i).flight.getDate() + "' , '" + VOP.SearchResponse.get(i).flight.getArrival_time() +
                            "' , '" + VOP.SearchResponse.get(i).flight.getDeparture_time() + "' , " + VOP.SearchResponse.get(i).flight.getFlight_no() + " , " + r1.getInt("id") + " ," +
                            r3.getInt("id") + " , " + r4.getInt("id") + " , " + r2.getInt("id") + " )";
                    con.execute_other_query(q5);
                    String q6 = "SELECT * FROM flight ORDER BY Id DESC";
                    ResultSet r6 = con.execute_select(q6);
                    r6.next();
                    flight_id = r6.getInt("id");
                    System.out.println("flight_id: " +flight_id);
                }
                else{
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    String q5 = "update flight set last_update_time = '" + timestamp + "' where id = " + rs.getInt("id") ;
                    con.execute_other_query(q5);
                    flight_id = rs.getInt("id");
                }
                for( int j = 0 ; j < VOP.SearchResponse.get(i).seatClasses.size() ; j++){
                    String q7 = "select * from seatclass where seatclass_code = '" +
                            VOP.SearchResponse.get(i).seatClasses.get(j).getSeat_class()+ "'" ;
                    ResultSet r7 = con.execute_select(q7);
                    r7.next();
                    int seatclass_id = r7.getInt("id");
                    System.out.println(seatclass_id);
                    String q8 = "select * from flight_seatclass where flight_id = " + flight_id + " and seatclass_id = " + seatclass_id ;
                    ResultSet r8 = con.execute_select(q8);
                    if(!r8.next()) {
                        String q9 = "insert into flight_seatclass (flight_id , seatclass_id , adult_price , child_price , infant_price , empty_count) " +
                                "values ( '" + flight_id + "' , '" + seatclass_id + "' , '" +
                                VOP.SearchResponse.get(i).seatClasses.get(j).getAdult_price() + "' , '" +
                                VOP.SearchResponse.get(i).seatClasses.get(j).getChild_price() + "' , '" +
                                VOP.SearchResponse.get(i).seatClasses.get(j).getInfant_price() + "' , '" +
                                VOP.SearchResponse.get(i).seatClasses.get(j).getCount() + "' )";
                        con.execute_other_query(q9);
                    }
                    else  {
                        String q9 = "update flight_seatclass set adult_price = " + VOP.SearchResponse.get(i).seatClasses.get(j).getAdult_price() +
                                " , child_price = " + VOP.SearchResponse.get(i).seatClasses.get(j).getChild_price() +
                                " , infant_price = " + VOP.SearchResponse.get(i).seatClasses.get(j).getInfant_price() +
                                " , empty_count = " + VOP.SearchResponse.get(i).seatClasses.get(j).getCount() +
                                " where flight_id = " + flight_id + " and seatclass_id = " + seatclass_id ;
                        con.execute_other_query(q9);
                    }
                }
            } catch (SQLException e) {
                System.out.println("Cannot connect the database! ..... " + e.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
