package DAOLayer.LoginDAO;

import DAOLayer.DBConnection;
import domain.VOUser;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Mehrad on 5/22/2017.
 */
public class LoginDAO {
    public int checkAccount(VOUser VOU){
        try {
            DBConnection con = new DBConnection();
            String q = "select ACCESSLEVEL from USER where USERNAME = ? and PASSWORD  = ? " ;
            ArrayList<String> params = new ArrayList<String>();
            params.add(VOU.username);
            params.add(VOU.password );
            ResultSet rs = con.execute_secure_select(q , params);
            if(rs.next()){
                return rs.getInt("ACCESSLEVEL");
            }else{
                return 0 ;
            }
        } catch (SQLException e) {
            System.out.println("Cannot connect the database! ..... " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0 ;
    }
}
