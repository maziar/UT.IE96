package domain;

/**
 * Created by goodarzysepideh on 11/02/2017.
 */
public class Flight {
    private String orig_code;
    private String dest_code;
    private String date;
    private String airline_code;
    private int flight_no;
    private String departure_time;
    private String arrival_time;
    private String airplane_model;

    public Flight(String orig_code, String dest_code, String date, String airline_code, int flight_no) {
        this.orig_code = orig_code;
        this.dest_code = dest_code;
        this.date = date;
        this.airline_code = airline_code;
        this.flight_no = flight_no;
    }

    public String getOrig_code() {
        return orig_code;
    }

    public String getDest_code() {
        return dest_code;
    }

    public String getDate() {
        return date;
    }

    public String getAirline_code() {
        return airline_code;
    }

    public int getFlight_no() {
        return flight_no;
    }

    public String get_filght_info() {
        return this.getOrig_code() + " " + this.getDest_code() + " " + this.getDate() + " " + this.getAirline_code() + " " + Integer.toString(this.getFlight_no());
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }

    public void setAirplane_model(String airplane_model) {
        this.airplane_model = airplane_model;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public String getArrival_time() {
        return arrival_time;
    }

    public String getAirplane_model() {
        return airplane_model;
    }

    public void setOrig_code(String orig_code) {
        this.orig_code = orig_code;
    }

    public void setDest_code(String dest_code) {
        this.dest_code = dest_code;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setAirline_code(String airline_code) {
        this.airline_code = airline_code;
    }

    public void setFlight_no(int flight_no) {
        this.flight_no = flight_no;
    }
}