package domain;

import DAOLayer.ReserveManagement.ReserveDAO;
import DAOLayer.SearchManagement.SearchDAO;
import DAOLayer.LoginDAO.LoginDAO;
import DAOLayer.TicketDAO.TicketDAO;
import domain.ReserveClass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by goodarzysepideh on 02/03/2017.
 */
public class Akbarticket {

    private ServerProvider sp;

    public Akbarticket(String apa_ip, int apa_port_no) throws IOException {
        sp = new ServerProvider(apa_ip, apa_port_no);
    }

    public void run() {
        System.out.println("hello");
    }

    public VOSearchResponses search(String from, String to, String depDate, String adultNum, String childNum, String infNum) throws IOException, ClassNotFoundException {
        VOSearchReq VOS = new VOSearchReq(from, to, depDate, adultNum, childNum, infNum);
        VOSearchResponses VOP = new VOSearchResponses();
        try {
            System.out.println("we are here 1=========================");
            SearchDAO searchDAO = new SearchDAO();
            VOP = searchDAO.search(VOS);
            System.out.println("we are here 2=========================");
            if (VOP.SearchResponse.size() == 0) {
                System.out.println("we are here 3===================");
                VOP = sp.search(VOS);
                searchDAO.update(VOP);
            }
        } catch (IOException ex) {
            //some error handling
        } finally {
            return VOP;
        }
    }

    public ReserveClass reserve(VOTempResReq votrr , String username) throws IOException {
        VOTempResRep res = sp.TemporalReserve(votrr);
        VOFinResReq req = new VOFinResReq();
        req.token = res.token;
        VOFinResRep final_res = sp.FinalReserve(req);
        ReserveDAO reserveDAO = new ReserveDAO();
        reserveDAO.final_reserve(final_res.reserve , username);
        return final_res.reserve;
    }

    public VOResultRep temp_reserve(VOResultReq VORR) {
        VOUpdatedPrice VOUP = sp.update_price(VORR);
        ReserveDAO reserveDAO = new ReserveDAO();
        VOResultRep VORRP = reserveDAO.temp_reserve(VORR, VOUP);
        return VORRP;
    }

    public int login(VOUser VOU) {
        LoginDAO loginDAO = new LoginDAO();
        return loginDAO.checkAccount(VOU);
    }

    public ArrayList<Ticket> getTickets(String username , String accessLevel){
        TicketDAO ticketDAO = new TicketDAO();
        ArrayList<Ticket>result = ticketDAO.getTickets(username , accessLevel);
        return result ;
    }

    public ArrayList<Ticket> getTicketById(String username , String accessLevel , int ticketID){
        TicketDAO ticketDAO = new TicketDAO();
        ArrayList<Ticket>result = ticketDAO.getTicketById(username , accessLevel , ticketID);
        return result ;
    }
}
