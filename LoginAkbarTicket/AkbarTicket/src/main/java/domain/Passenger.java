package domain;

/**
 * Created by monsieur maaz on 2/9/2017.
 */
public class Passenger {
    public String FirstName;
    public String SurName;
    public String NationalID;
    public String sex;
    public Ticket ticket;

    public Passenger(){

    }
    public Passenger(String fn, String sn, String ni, String sx){
        FirstName = fn;
        SurName = sn;
        NationalID = ni;
        sex = sx ;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public void setNationalID(String nationalID) {
        NationalID = nationalID;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getSurName() {
        return SurName;
    }

    public String getNationalID() {
        return NationalID;
    }

    public String getSex() {
        return sex;
    }

    String get_passenger_info() {
        return FirstName + " " + SurName + " " + NationalID + "\n";
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Ticket getTicket() {
        return ticket;
    }
}
