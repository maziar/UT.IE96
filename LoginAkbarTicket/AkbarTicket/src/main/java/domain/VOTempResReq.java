package domain;

import domain.Passenger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by goodarzysepideh on 03/03/2017.
 */
public class VOTempResReq {
    public String orig_code ;
    public String dest_code ;
    public String departure_time;
    public String arival_time ;
    public String airplane_model;
    public String date ;
    public String airline_code ;
    public String flight_no ;
    public String seat_class ;
    public String adult_count ;
    public String adult_price ;
    public String child_count ;
    public String child_price ;
    public String infant_count ;
    public String infant_price ;
    public String total_price ;
    public String num_of_seats ;
    public List<Passenger> passengers;
    public String token ;
}
