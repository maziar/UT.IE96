package domain;

/**
 * Created by Mehrad on 5/11/2017.
 */
public class VOResultRep {
    public String airline_code ;
    public int flight_number ;
    public String date ;
    public String orig_code ;
    public String dest_code ;
    public String departure_time ;
    public String arival_time ;
    public String airplane_model ;
    public String seat_class ;
    public int adult_count ;
    public int adult_price ;
    public int child_count ;
    public int child_price ;
    public int infant_count ;
    public int infant_price ;
    public int total_price ;
    public int num_of_seats ;
    public VOConverted converted ;
    public String token ;
}
