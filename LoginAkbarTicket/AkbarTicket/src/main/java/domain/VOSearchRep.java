package domain;

import domain.Flight;
import domain.SeatClass;

import java.util.ArrayList;

/**
 * Created by goodarzysepideh on 02/03/2017.
 */
public class VOSearchRep {
    public Flight flight ;
    public ArrayList <Integer> total_prices ;
    public ArrayList <SeatClass> seatClasses ;
    public int adult_cnt;
    public int child_cnt;
    public int inf_cnt;

    public VOSearchRep(Flight flight, ArrayList<Integer> total_prices, ArrayList<SeatClass> seatClasses, int adult_cnt, int child_cnt, int inf_cnt) {
        this.flight = flight;
        this.total_prices = total_prices;
        this.seatClasses = seatClasses;
        this.adult_cnt = adult_cnt;
        this.child_cnt = child_cnt;
        this.inf_cnt = inf_cnt;
    }
}
