package domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by monsieur maaz on 3/2/2017.
 */
public class FlightRepo {
    private List<Flight> Flights;

    private static FlightRepo flight_repo = new FlightRepo();

    private FlightRepo () {
        Flights = new ArrayList<Flight>();
    }

    public static FlightRepo getFlight_repo() {return flight_repo;}

    public List<Flight> getFlights() { return Flights;}

    public void add_flight (Flight F) {
        Flights.add(F);
    }

    public void setDeparture_time(int flight , String departure_time){
        Flights.get(flight).setDeparture_time(departure_time);
    }

    public void setArrival_time(int flight , String arrival_time){
        Flights.get(flight).setArrival_time(arrival_time);
    }

    public  void setAirplane_model(int flight , String airplane_model){
        Flights.get(flight).setAirplane_model(airplane_model);
    }

    public int searchForFlight(Flight F){
        int res = -1 ;
        for(int i = 0 ; i < Flights.size() ; i++){
            if(Flights.get(i).get_filght_info().equals(F.get_filght_info())){
                res = i ;
            }
        }
        return res ;
    }

}
