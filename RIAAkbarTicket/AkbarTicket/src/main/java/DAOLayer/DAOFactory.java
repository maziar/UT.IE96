package DAOLayer;

import DAOLayer.ReserveManagement.ReserveDAOFactory;
import DAOLayer.SearchManagement.SearchDAOFactory;

/**
 * Created by monsieur maaz on 5/12/2017.
 */
public abstract class DAOFactory {
    private static final int __PRODUCTION__ = 1;
    private static final int __TEST__ = 2;
    private static final int __MODE__ = __PRODUCTION__;

    public abstract ReserveDAOFactory getReserveDAO();
    public abstract SearchDAOFactory getSearchDAO();

    public static DAOFactory getDAOFactory () {
        switch (__MODE__) {
            case __PRODUCTION__:
                 return new ProductionDAOFactory();
            case __TEST__:
                return new TestDAOFactory();
            default: return null;
        }
    }

}
