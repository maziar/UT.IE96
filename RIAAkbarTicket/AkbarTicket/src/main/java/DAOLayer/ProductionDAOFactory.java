package DAOLayer;

import DAOLayer.ReserveManagement.ReserveDAO;
import DAOLayer.ReserveManagement.ReserveDAOFactory;
import DAOLayer.SearchManagement.SearchDAO;
import DAOLayer.SearchManagement.SearchDAOFactory;

/**
 * Created by monsieur maaz on 5/12/2017.
 */
public class ProductionDAOFactory extends DAOFactory {
    public ReserveDAOFactory getReserveDAO(){
        return new ReserveDAO();
    }
    public SearchDAOFactory getSearchDAO(){
        return new SearchDAO();
    }
}
