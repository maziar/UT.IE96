package DAOLayer;

import DAOLayer.ReserveManagement.ReserveDAOFactory;
import DAOLayer.ReserveManagement.TestReserveDAO;
import DAOLayer.SearchManagement.SearchDAOFactory;
import DAOLayer.SearchManagement.TestSearchDAO;

/**
 * Created by monsieur maaz on 5/12/2017.
 */
public class TestDAOFactory extends DAOFactory {
    public ReserveDAOFactory getReserveDAO(){
        return new TestReserveDAO();
    }
    public SearchDAOFactory getSearchDAO(){
        return new TestSearchDAO();
    }
}
