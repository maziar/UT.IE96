package DAOLayer;

import com.sun.java.util.jar.pack.*;
import service.searchManagement.VOSearchReq;


import java.sql.Statement;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by monsieur maaz on 5/8/2017.
 */
public class DBConnection {
    Connection con;
    public DBConnection() throws IOException, SQLException {
        con =  null;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/AkbarDB", "SA", "");
            if (con != null){
                System.out.println("Connection Created succesfully ... ");
            }else {
                System.out.println("Connection fucked up!");
            }

        }catch (Exception e){
            System.out.println("Ridim!");
        }
    }


    public ResultSet execute_select(String query) throws SQLException {
        System.out.println("Query is : " + query);
        Statement statement =  con.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        statement.close();
        return resultSet;
    }

    public void execute_other_query(String query)throws SQLException {
        System.out.println("Query is : " + query);
        Statement statement =  con.createStatement();
        statement.executeQuery(query);
        statement.close();
    }
}
