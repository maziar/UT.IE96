package DAOLayer.ReserveManagement;

import domain.ReserveClass;
import domain.VOResultRep;
import domain.VOResultReq;
import domain.VOUpdatedPrice;

/**
 * Created by monsieur maaz on 5/12/2017.
 */
public interface ReserveDAOFactory {
    public VOResultRep temp_reserve(VOResultReq VORR, VOUpdatedPrice VOUP);
    public void final_reserve(ReserveClass rc);
}
