package DAOLayer.ReserveManagement;

import DAOLayer.DBConnection;
import domain.ReserveClass;
import domain.VOResultRep;
import domain.VOResultReq;
import domain.VOUpdatedPrice;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Mehrad on 5/11/2017.
 */
public class ReserveDAO implements ReserveDAOFactory{
    public VOResultRep temp_reserve(VOResultReq VORR , VOUpdatedPrice VOUP){
        VOResultRep VORRP = new VOResultRep();
        try {
            DBConnection con = new DBConnection();
            String q = "select id from seatclass where seatclass_code = '" + VORR.seat_class + "'" ;
            ResultSet rs = con.execute_select(q);
            rs.next();
            int seatclass_id = rs.getInt("id") ;
            q = "select id from city where city_code = '" + VORR.orig_code + "'" ;
            rs = con.execute_select(q);
            rs.next();
            int orig_city_id = rs.getInt("id");
            q = "select id from city where city_code = '" + VORR.dest_code + "'" ;
            rs = con.execute_select(q);
            rs.next();
            int dest_city_id = rs.getInt("id");
            q = "select id from airline where airline_code = '" + VORR.airline_code + "'" ;
            rs = con.execute_select(q);
            rs.next();
            int airline_id = rs.getInt("id");
            q = "select id from airplane where airplane_model = '" + VORR.airplane_model + "'" ;
            rs = con.execute_select(q);
            rs.next();
            int airplane_id = rs.getInt("id");
            q = "SELECT id" +
                    " FROM flight" +
                    " INNER JOIN flight_seatclass" +
                    " ON flight.id = flight_seatclass.flight_id where date = '" + VORR.date + "' and arrival_time = '" + VORR.arival_time + "'" +
                    " and departure_time = '" + VORR.departure_time + "' and flight_number = " + VORR.flight_number + " and orig_city_id = " +
                    orig_city_id + " and airline_id = " + airline_id + " and airplane_id = " + airplane_id + " and dest_city_id = " + dest_city_id + "" +
                    " and seatclass_id = " + seatclass_id ;
            rs = con.execute_select(q);
            rs.next();
            int flight_id = rs.getInt("id");
            q = "insert into reserve (seatclass_id , flight_id , adult_count , child_count , infant_count , total_price , adult_price , child_price , infant_price) values" +
                    "(" + seatclass_id + " , "+ flight_id +" , "+ VORR.adult_count +" , "+ VORR.child_count +" , "+
                    VORR.infant_count +" , "+ VORR.total_price+" , " + VOUP.adult_price + " , " + VOUP.child_price + " , " +
                    VOUP.infant_price + ")" ;
            con.execute_other_query(q);
        } catch (SQLException e) {
            System.out.println("Cannot connect the database! ..... " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        VORRP.airline_code = VORR.airline_code ;
        VORRP.flight_number = VORR.flight_number ;
        VORRP.date = VORR.date ;
        VORRP.orig_code = VORR.orig_code ;
        VORRP.dest_code = VORR.dest_code ;
        VORRP.departure_time = VORR.departure_time ;
        VORRP.arival_time = VORR.arival_time ;
        VORRP.airplane_model = VORR.airplane_model ;
        VORRP.seat_class = VORR.seat_class ;
        VORRP.adult_count = VORR.adult_count ;
        VORRP.child_count = VORR.child_count ;
        VORRP.infant_count = VORR.infant_count ;
        VORRP.num_of_seats = VORR.num_of_seats ;
        VORRP.converted = VORR.converted ;
        VORRP.adult_price = VOUP.adult_price ;
        VORRP.child_price = VOUP.child_price ;
        VORRP.infant_price = VOUP.infant_price ;
        VORRP.total_price = VORRP.adult_price * VORRP.adult_count +
                VORRP.child_price * VORRP.child_count +
                VORRP.infant_price * VORRP.infant_count ;
        return VORRP ;
    }

    public void final_reserve(ReserveClass rc){
        try {
            DBConnection con = new DBConnection();
            String q = "select id from seatclass where seatclass_code = '" + rc.getSeat_class().getSeat_class() + "'" ;
            ResultSet rs = con.execute_select(q);
            rs.next();
            int seatclass_id = rs.getInt("id") ;
            q = "select id from city where city_code = '" + rc.getFlight().getOrig_code() + "'" ;
            rs = con.execute_select(q);
            rs.next();
            int orig_city_id = rs.getInt("id");
            q = "select id from city where city_code = '" + rc.getFlight().getDest_code() + "'" ;
            rs = con.execute_select(q);
            rs.next();
            int dest_city_id = rs.getInt("id");
            q = "select id from airline where airline_code = '" + rc.getFlight().getAirline_code() + "'" ;
            rs = con.execute_select(q);
            rs.next();
            int airline_id = rs.getInt("id");
            q = "select id from airplane where airplane_model = '" + rc.getFlight().getAirplane_model() + "'" ;
            rs = con.execute_select(q);
            rs.next();
            int airplane_id = rs.getInt("id");
            q = "SELECT id" +
                    " FROM flight" +
                    " INNER JOIN flight_seatclass" +
                    " ON flight.id = flight_seatclass.flight_id where date = '" + rc.getFlight().getDate() + "' and arrival_time = '" + rc.getFlight().getArrival_time() + "'" +
                    " and departure_time = '" + rc.getFlight().getDeparture_time() + "' and flight_number = " + rc.getFlight().getFlight_no() + " and orig_city_id = " +
                    orig_city_id + " and airline_id = " + airline_id + " and airplane_id = " + airplane_id + " and dest_city_id = " + dest_city_id + "" +
                    " and seatclass_id = " + seatclass_id ;
            rs = con.execute_select(q);
            rs.next();
            int flight_id = rs.getInt("id");
            q = "select * from reserve where flight_id = " + flight_id + " and seatclass_id = " +
                    seatclass_id + " and token is NULL and ref_code is NULL" ;
            rs = con.execute_select(q);
            rs.next();
            int reserve_id = rs.getInt("id");
            q = "update reserve set adult_count = " + rc.getAdult_cnt() +
                    " , child_count = " + rc.getChild_cnt() +
                    " , infant_count = " + rc.getInfant_cnt() +
                    " , token = '" + rc.getToken() +
                    "' , total_price = " + rc.getTotal_price() +
                    " , ref_code = '" + rc.getRef_code() +
                    "' , adult_price = " + rc.getSeat_class().getAdult_price() +
                    " , child_price = " + rc.getSeat_class().getChild_price() +
                    " , infant_price = " + rc.getSeat_class().getInfant_price() +
                    " where flight_id = " + flight_id + " and seatclass_id = " +
                    seatclass_id ;
            con.execute_other_query(q);
            for(int i = 0 ; i < rc.getPassengers().size() ; i++){
                q = "insert into passengers (firstname , surname , nationalid , sex , ticket_no , reserve_id) values" +
                        "( '" + rc.getPassengers().get(i).getFirstName() + "' , '" + rc.getPassengers().get(i).getSurName()+ "' , '" + rc.getPassengers().get(i).getNationalID() + "' , '" + rc.getPassengers().get(i).getSex() + "' , '" + rc.getPassengers().get(i).getTicket() + "' , " + reserve_id + ")" ;
                con.execute_other_query(q);
            }
        } catch (SQLException e) {
            System.out.println("Cannot connect the database! ..... " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
