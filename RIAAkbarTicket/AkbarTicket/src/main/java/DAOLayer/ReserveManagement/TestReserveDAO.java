package DAOLayer.ReserveManagement;

import domain.*;

/**
 * Created by monsieur maaz on 5/12/2017.
 */
public class TestReserveDAO implements ReserveDAOFactory {
    public VOResultRep temp_reserve(VOResultReq VORR, VOUpdatedPrice VOUP){
        VOResultRep VOR = new VOResultRep();
        VOR.airline_code = "IR";
        VOR.flight_number = 452;
        VOR.date = "05Feb";
        VOR.orig_code = "THR";
        VOR.dest_code = "MHD";
        VOR.departure_time = "17:30";
        VOR.arival_time = "18:40";
        VOR.airplane_model = "M80";
        VOR.seat_class = "Y";
        VOR.adult_count = 1;
        VOR.adult_price = 2000;
        VOR.child_count = 1;
        VOR.child_price = 2000;
        VOR.infant_count = 1;
        VOR.infant_price  = 2000;
        VOR.total_price = 6000;
        VOR.num_of_seats = 8;
        VOR.converted = new VOConverted();
        return VOR;
    }
    public void final_reserve(ReserveClass rc) {

    }
}
