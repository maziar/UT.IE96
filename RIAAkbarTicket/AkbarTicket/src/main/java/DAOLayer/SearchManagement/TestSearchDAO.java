package DAOLayer.SearchManagement;

import domain.*;

import java.util.ArrayList;

/**
 * Created by monsieur maaz on 5/12/2017.
 */
public class TestSearchDAO implements SearchDAOFactory {
    public VOSearchResponses search(VOSearchReq VOSRQ){
        VOSearchResponses VOSRPS = new VOSearchResponses();
        Flight f = new Flight("THR", "MHD", "05Feb", "IR", 452);
        ArrayList<Integer> total_prices = new ArrayList<Integer>();
        total_prices.add(6000);
        SeatClass sc = new SeatClass("Y");
        ArrayList<SeatClass> seat_classes = new ArrayList<SeatClass>();
        seat_classes.add(sc);
        VOSearchRep VOSRP = new VOSearchRep(f, total_prices,seat_classes,1 ,1 ,1);
        VOSRPS.addVOSearchResponses(VOSRP);
        return VOSRPS;
    }
    public void update (VOSearchResponses VOP){ }
}
