package DAOLayer.SearchManagement;

import domain.VOSearchReq;
import domain.VOSearchResponses;

/**
 * Created by monsieur maaz on 5/12/2017.
 */
public interface SearchDAOFactory {
    public VOSearchResponses search(VOSearchReq VOSRQ);
    public void update (VOSearchResponses VOP);
}
