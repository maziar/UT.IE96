package domain;

import DAOLayer.DAOFactory;
import DAOLayer.ReserveManagement.ReserveDAO;
import DAOLayer.ReserveManagement.ReserveDAOFactory;
import DAOLayer.SearchManagement.SearchDAO;
import DAOLayer.SearchManagement.SearchDAOFactory;
import domain.ReserveClass;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Created by goodarzysepideh on 02/03/2017.
 */
public class Akbarticket {
    private static final Logger logger = Logger.getLogger(Akbarticket.class);
    private ServerProvider sp ;

    public Akbarticket(){}

    public Akbarticket( String apa_ip , int apa_port_no) throws IOException{
        sp = new ServerProvider(apa_ip , apa_port_no);
    }

    public void run(){
        System.out.println("hello");
    }

    public VOSearchResponses search(String from, String to, String depDate, String adultNum, String childNum, String infNum) throws IOException, ClassNotFoundException{
        VOSearchReq VOS = new VOSearchReq(from, to, depDate, adultNum, childNum, infNum);
        VOSearchResponses VOP = new VOSearchResponses();
        try {
            System.out.println("we are here 1=========================");
            /*SearchDAO searchDAO = new SearchDAO();*/
            DAOFactory DOF = DAOFactory.getDAOFactory();
            SearchDAOFactory searchDAO = DOF.getSearchDAO();
            VOP = searchDAO.search(VOS);
            logger.info("Search in DB");
            System.out.println("we are here 2=========================");
            if (VOP.SearchResponse.size() == 0) {
                System.out.println("we are here 3===================");
                VOP = sp.search(VOS);
                searchDAO.update(VOP);
            }
        }catch(IOException ex) {
            //some error handling
        }
        finally {
            return VOP;
        }
    }

    public ReserveClass reserve(VOTempResReq votrr)throws IOException{
        VOTempResRep res = sp.TemporalReserve(votrr);
        VOFinResReq req = new VOFinResReq();
        req.token = res.token ;
        VOFinResRep final_res = sp.FinalReserve(req);
        DAOFactory DOF = DAOFactory.getDAOFactory();
        ReserveDAOFactory reserveDAO = DOF.getReserveDAO();
        /*ReserveDAO reserveDAO = new ReserveDAO();*/
        reserveDAO.final_reserve(final_res.reserve);
        logger.info("Reserve done");
        return final_res.reserve ;
    }

    public VOResultRep temp_reserve(VOResultReq VORR) {
        VOUpdatedPrice VOUP = sp.update_price(VORR);
        DAOFactory DOF = DAOFactory.getDAOFactory();
        ReserveDAOFactory reserveDAO = DOF.getReserveDAO();
        /*ReserveDAO reserveDAO = new ReserveDAO();*/
        VOResultRep VORRP = reserveDAO.temp_reserve(VORR , VOUP);
        return VORRP ;
    }

}
