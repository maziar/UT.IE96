package domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by monsieur maaz on 3/2/2017.
 */
public class ReserveRepo {
    private List<ReserveClass> reserves;

    private static ReserveRepo reserveRepo = new ReserveRepo();

    private ReserveRepo () {
        reserves = new ArrayList<ReserveClass>();
    }

    public static ReserveRepo getReserveRepo() {return reserveRepo;}

    public List<ReserveClass> getReserves() { return reserves;}

    public void add_reserve (ReserveClass R) {
        reserves.add(R);
    }

    public void set_flight_additional_info(int i , String departure_time, String arrival_time, String airplane_model){
        reserves.get(i).set_flight_additional_info(departure_time , arrival_time , airplane_model);
    }

    public void setRef_code(int i , String ref_code){
        reserves.get(i).setRef_code(ref_code);
    }

    public void set_pass_ticktNo(int i , int k, Ticket ticket){
        reserves.get(i).set_pass_ticktNo(k , ticket);
    }
}
