package domain;

import domain.*;

import java.io.*;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by goodarzysepideh on 02/03/2017.
 */
public class ServerProvider {
    private String apa_ip;
    private int apa_port_no ;
    private Socket as_client;

    public ServerProvider(String apa_ip , int apa_port_no) throws IOException{
        this.apa_ip = apa_ip;
        this.apa_port_no = apa_port_no ;
        InetAddress apa_addr = InetAddress.getByName(apa_ip);
        while(true) {
            try {
                as_client = new Socket(apa_addr, apa_port_no);
                break;
            } catch (ConnectException e) {
                continue;
            }
        }
    }

    public static int calc_price(int adult, int child, int infant, String res) throws ArrayIndexOutOfBoundsException{
        String[] splitted_res = res.split(" ");
        int adult_p = Integer.parseInt(splitted_res[0]);
        int child_p = Integer.parseInt(splitted_res[1]);
        int infant_p = Integer.parseInt(splitted_res[2]);
        if(adult < 0 || child < 0 || infant < 0 || (adult == 0 && child == 0 && infant == 0)){
            return -1 ;
        }
        return adult * adult_p + child * child_p + infant * infant_p;
    }

    public VOSearchResponses process_res(ArrayList<String> response, String adult_cnt, String child_cnt, String infant_cnt , String date) throws IOException {
        System.out.println("[dbg] in process result we have the fucking error!: " + "adult count: " +adult_cnt + "size of adult count is : " + adult_cnt.length() + "child count: " +child_cnt);
        int adult = 0;
        int child = 0;
        int infant = 0;

        try{
            adult = Integer.parseInt(adult_cnt);
            child = Integer.parseInt(child_cnt);
            infant = Integer.parseInt(infant_cnt);
        }
        catch (NumberFormatException NFX){
            NFX.printStackTrace();
        }

        System.out.println("[dbg] in process result we have the fucking error!");
        String res;
        BufferedReader in = new BufferedReader(
                new InputStreamReader(as_client.getInputStream()));
        PrintWriter out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(as_client.getOutputStream())),true);
        VOSearchResponses result = new VOSearchResponses();
        System.out.println("[dbg] in process result we have the fucking error!");
        for (int i = 0; i < response.size(); i+=2){
            String[] temp = response.get(i).split(" ");
            String airline_code = "";
            String flight_no = "";
            String orig_code = "";
            String dest_code = "";
            String departure_time = "";
            String arrival_time = "";
            String airplane_mod = "";
            try {
                airline_code = temp[0];
                flight_no = temp[1];
                orig_code = temp[3];
                dest_code = temp[4];
                departure_time = temp[5].substring(0, 2) + ":" + temp[5].substring(2);
                arrival_time = temp[6].substring(0, 2) + ":" + temp[6].substring(2);
                airplane_mod = temp[7];
            }
            catch (ArrayIndexOutOfBoundsException e){
                //return "" ;
            }
            String[] seat_classes = response.get(i+1).split(" ");
            System.out.println("[dbg] seat classes: " + seat_classes.length);
            ArrayList<SeatClass>classes = new ArrayList<SeatClass>();
            ArrayList<Integer>total_prices = new ArrayList<Integer>();
            for (int j = 0; j < seat_classes.length; j++){
                boolean is_avail = false;
                int count = 0 ;
                if (seat_classes[j].charAt(1) != 'A' && seat_classes[j].charAt(1) != 'C') {
                    count = Character.getNumericValue(seat_classes[j].charAt(1));
                    if (count >= adult + child + infant){
                        System.out.println("[dbg] we are here!!");
                        is_avail = true;
                    }
                }
                else if (seat_classes[j].charAt(1) == 'A'){
                    count = 9 ;
                    System.out.println("[dbg] we are here!!");
                    is_avail = true;
                }

                if (is_avail == true){
                    String req_query = "PRICE" + " " + orig_code + " " + dest_code + " " + airline_code + " " + seat_classes[j].charAt(0);
                    System.out.println("requested query is : " + req_query);
                    res = "" ;
                    try {
                        out.println(req_query);
                        res = in.readLine();
                    }
                    catch (IOException e){
                        //return "" ;
                    }
                    System.out.println("price query result is : " + res);
                    try {
                        int price = calc_price(adult , child , infant , res);
                        if(price == -1){
                            //return "bad request(wrong passengers number)!!!";
                        }
                        SeatClass s = new SeatClass(Character.toString(seat_classes[j].charAt(0)));
                        String[] prices = res.split(" ");
                        s.setAdult_price(Integer.parseInt(prices[0]));
                        s.setChild_price(Integer.parseInt(prices[1]));
                        s.setInfant_price(Integer.parseInt(prices[2]));
                        s.setCount(count);
                        classes.add(s);
                        total_prices.add(price);
                    }
                    catch (ArrayIndexOutOfBoundsException e){
                        //return "";
                    }
                }
            }
            if (classes.size() != 0 ){
                Flight f = new Flight(orig_code , dest_code , date , airline_code , Integer.parseInt(flight_no));
                f.setDeparture_time(departure_time);
                f.setArrival_time(arrival_time);
                f.setAirplane_model(airplane_mod);
                VOSearchRep final_res = new VOSearchRep(f, total_prices, classes, adult, child, infant);

                //airline _code, flight_no, date, orig_code, dest_code, dep_time, arr_time, airplane_model, seat_class,
                //adult_count, adult_price, child_count, child_price, infant_count, infant_price, total
                // for my view i want count too
                result.addVOSearchResponses(final_res);
            }
        }
        return result ;
    }

    public VOSearchResponses search(VOSearchReq vosq) throws IOException{
        ArrayList<String> response = new ArrayList<String>();
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(as_client.getInputStream()));
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new OutputStreamWriter(as_client.getOutputStream())), true);
            String req_query = "AV" + " " + vosq.orig_code + " " + vosq.dest_code + " " + vosq.date;
            System.out.println("[dbg] request query is: " + req_query);
            out.println(req_query);
            String p_res;
            do {
                p_res = in.readLine();
                response.add(p_res);
                System.out.println("[dbg] the partial response form server is: " + p_res);
                System.out.println("[dbg] size of partial res: " + p_res.length());
            } while (in.ready());
        }
        catch (IOException e){
            System.out.println("[dbg] error in the socket with the server");
            //return "server is down , pls try again later , thanks for you being patient ";
        }
        System.out.println("[dbg] out from while ?");
        VOSearchResponses final_res = process_res(response, vosq.adult_count, vosq.child_count, vosq.infant_cout , vosq.date);
        return final_res ;

    }

    public VOTempResRep TemporalReserve(VOTempResReq votrq)throws IOException{
        VOTempResRep final_res = new VOTempResRep();
        int adult = Integer.parseInt(votrq.adult_count);
        int child = Integer.parseInt(votrq.child_count);
        int infant = Integer.parseInt(votrq.infant_count);
        int flight_num = Integer.parseInt(votrq.flight_no);
        int total_pass = adult + child + infant;
        String response = "" ;
        System.out.println("[dbg] total passengers is: " + total_pass);
        Flight F = new Flight(votrq.orig_code,  votrq.dest_code, votrq.date, votrq.airline_code,  flight_num);
        int flight_key = FlightRepo.getFlight_repo().searchForFlight(F);
        if(flight_key == -1){
            FlightRepo.getFlight_repo().add_flight(F);
        }
        flight_key = FlightRepo.getFlight_repo().getFlights().size() - 1 ;
        SeatClass S = new SeatClass(votrq.seat_class);
        ReserveClass R = new ReserveClass(flight_key, S, adult, child, infant);
        System.out.println("[dbg] the reserver info is : " + R.get_info());
        String query = "RES " + R.get_info();
        /*BufferedReader in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));*/
        BufferedReader in_from_server = new BufferedReader(
                new InputStreamReader(as_client.getInputStream()));
        PrintWriter out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(as_client.getOutputStream())),true);
        System.out.println("[dbg] out loop query is: " + query);
        out.flush();
        try {
            out.println(query);
            for (int i = 0; i < total_pass; i++) {
                out.flush();
                R.add_passenger(votrq.passengers.get(i));
                query = R.getPassengers().get(i).getFirstName() + " " + R.getPassengers().get(i).getSurName()
                        + " " + R.getPassengers().get(i).getNationalID();
                System.out.println("[dbg] loop query: " + query);
                out.println(query);
                System.out.println("[dbg] adding passenger ...");
            }
            response = in_from_server.readLine();
            //query = "RES " + R.get_info() + R.get_list_of_passengers().substring(0, R.get_list_of_passengers().length()-1);
            //System.out.println("[dbg] the reserve querygo to server is : \n" + query);
            //out.println(query);
        }
        catch (IOException e){
            //return "server is down , pls try again later , thanks for you being patient";
        }
        System.out.println("[dbg] the response of reserve from server is : " + response);
        String[] response_params = response.split(" ");
        try {
            int total_price = calc_price(adult, child, infant, response.substring(response.indexOf(" ") + 1, response.length()));
            if(total_price == -1){
                //return "bad request(wrong passengers number)!!!";
            }
            R.setToken(response.substring(0, response.indexOf(" ")));
            R.setTotal_price(total_price);
            R.set_seat_class(Integer.parseInt(response_params[1]), Integer.parseInt(response_params[2]), Integer.parseInt(response_params[3]));
            ReserveRepo.getReserveRepo().add_reserve(R);
            final_res.token = R.getToken();
            final_res.total_price = R.getTotal_price();
        }
        catch (ArrayIndexOutOfBoundsException e){
            //return "server is down , pls try again later , thanks for you being patient";
        }
        return final_res ;
    }
    public VOFinResRep FinalReserve(VOFinResReq vofrq) throws  IOException{
        VOFinResRep finalized_res = new VOFinResRep() ;
        for(int i = 0 ; i < ReserveRepo.getReserveRepo().getReserves().size() ; i++){
            if(ReserveRepo.getReserveRepo().getReserves().get(i).getToken().equals(vofrq.token)){
                String orig_code = ReserveRepo.getReserveRepo().getReserves().get(i).getFlight().getOrig_code();
                String dest_code = ReserveRepo.getReserveRepo().getReserves().get(i).getFlight().getDest_code();
                String date  = ReserveRepo.getReserveRepo().getReserves().get(i).getFlight().getDate();
                String airline_code  = ReserveRepo.getReserveRepo().getReserves().get(i).getFlight().getAirline_code();
                String flight_no  = Integer.toString(ReserveRepo.getReserveRepo().getReserves().get(i).getFlight().getFlight_no());
                String seat_class = ReserveRepo.getReserveRepo().getReserves().get(i).getSeat_class().getSeat_class();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(as_client.getInputStream()));
                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(as_client.getOutputStream())),true);
                out.println("AV " + orig_code + " " + dest_code + " " + date);
                String p_res;
                String response = "";
                try {
                    ArrayList<String> flights = new ArrayList<String>();
                    do {
                        p_res = in.readLine();
                        flights.add(p_res);
                    } while (in.ready());
                    System.out.println(response);
                    for (int j = 0; j < flights.size(); j += 2) {
                        String[] flights_params = flights.get(j).split(" ");
                        System.out.println("[dbg] flights_params[0] " + flights_params[0]);
                        System.out.println("[dbg] airline_code " + airline_code);
                        System.out.println("[dbg] flights_params[1] " + flights_params[1]);
                        System.out.println("[dbg] flight_no " + flight_no);
                        try {
                            if (flights_params[0].equals(airline_code) && flights_params[1].equals(flight_no)) {
                                System.out.println("[dbg] we are absolutley here");
                                convertor con = new convertor();
                                String departure_time = con.convert_time_special(flights_params[5]);
                                String arrival_time = con.convert_time_special(flights_params[6]);
                                String airplane_model = flights_params[7];
                                ReserveRepo.getReserveRepo().set_flight_additional_info(i, departure_time, arrival_time, airplane_model);
                                out.println("FIN " + vofrq.token);
                                ArrayList<Passenger> passengers = ReserveRepo.getReserveRepo().getReserves().get(i).getPassengers();
                                String ref_code = in.readLine();
                                ReserveRepo.getReserveRepo().setRef_code(i, ref_code);
                                System.out.println("[dbg] the reference code is : " + ref_code);
                                String ticket_no = "";
                                for (int k = 0; k < passengers.size(); k++) {
                                    ticket_no = in.readLine();
                                    System.out.println("[dbg] ticket_no: " + ticket_no);
                                    Ticket ticket = new Ticket(ticket_no);
                                    ReserveRepo.getReserveRepo().set_pass_ticktNo(i, k, ticket);
                                }
                                finalized_res.reserve = ReserveRepo.getReserveRepo().getReserves().get(i);
                                break;
                            }
                        }catch (ArrayIndexOutOfBoundsException e){
                            //return "server is down , pls try again later , thanks for you being patient";
                        }
                    }
                }
                catch (IOException e){
                    //return "server is down , pls try again later , thanks for you being patient";
                }
            }
        }
        //System.out.println("[dbg] the last response is : \n" + finalized_res);
        return  finalized_res ;
    }

    public VOUpdatedPrice update_price(VOResultReq VORR){
        String p_res = "";
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(as_client.getInputStream()));
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new OutputStreamWriter(as_client.getOutputStream())), true);
            String req_query = "PRICE "  + VORR.orig_code + " " + VORR.dest_code + " " + VORR.airline_code + " " + VORR.seat_class ;
            System.out.println("[dbg] request query is: " + req_query);
            out.println(req_query);
            do {
                p_res = in.readLine();
                System.out.println("[dbg] the partial response form server is: " + p_res);
                System.out.println("[dbg] size of partial res: " + p_res.length());
            } while (in.ready());
        }
        catch (IOException e){
            System.out.println("[dbg] error in the socket with the server");
            //return "server is down , pls try again later , thanks for you being patient ";
        }
        System.out.println("[dbg] out from while ?");
        VOUpdatedPrice final_res = new VOUpdatedPrice();
        String[] prices = p_res.split(" ");
        final_res.adult_price = Integer.parseInt(prices[0]);
        final_res.child_price = Integer.parseInt(prices[1]);
        final_res.infant_price = Integer.parseInt(prices[2]);
        return final_res ;
    }
}

