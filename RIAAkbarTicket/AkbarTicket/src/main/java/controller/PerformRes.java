package controller;

import domain.VOConverted;
import domain.convertor;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by goodarzysepideh on 04/03/2017.
 */
public class PerformRes extends HttpServlet {
        private static final Logger logger = Logger.getLogger(PerformRes.class);
        public void init() throws ServletException {

        }
        public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*try {

        }catch (){

        }*/
        }

        public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
            convertor conv =  new convertor();
            VOConverted voc = conv.convert(request.getParameter("airline_code") ,
                    request.getParameter("orig_code") ,
                    request.getParameter("dest_code") ,
                    request.getParameter("airplane_model") ,
                    request.getParameter("seat_class") ,
                    request.getParameter("date"),
                    request.getParameter("departure_time"),
                    request.getParameter("arrival_time")
            );
            String flightID = request.getParameter("flight_no")+"#"+request.getParameter("orig_code")+"#"+request.getParameter("dest_code")+"#"+request.getParameter("date");
            String adultp = request.getParameter("adult_price");
            String childp = request.getParameter("child_price");
            String infantp = request.getParameter("infant_price");
            logger.info("TMPRES" + " " + flightID + " " + adultp + " " + childp + " " + infantp);
            request.setAttribute("converted" , voc);
            request.getRequestDispatcher("Reserve.jsp").forward(request, response);
        }

        public void destroy(){


        }
}
