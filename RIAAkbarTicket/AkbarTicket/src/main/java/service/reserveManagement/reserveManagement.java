package service.reserveManagement;

import domain.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Created by goodarzysepideh on 27/04/2017.
 */
@Path("/reserveManagement")
public class reserveManagement {
    @POST
    @Path("/reserve")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public VOFinResRep search(VOTempResReq VOTRQ) throws IOException, ClassNotFoundException {
        Akbarticket Akbar = new Akbarticket("178.62.207.47", 8081);
        convertor con = new convertor();
        ReserveClass RC = Akbar.reserve(VOTRQ);
        VOConverted voc = con.convert(VOTRQ.airline_code , VOTRQ.orig_code , VOTRQ.dest_code , VOTRQ.airplane_model ,
                VOTRQ.seat_class , VOTRQ.date , VOTRQ.departure_time , VOTRQ.arival_time );
        VOFinResRep VOFRP = new VOFinResRep();
        VOFRP.reserve = RC ;
        VOFRP.converted = voc ;
        return VOFRP ;
    }
}
