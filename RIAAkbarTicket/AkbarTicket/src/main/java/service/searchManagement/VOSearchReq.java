package service.searchManagement;

/**
 * Created by monsieur maaz on 4/20/2017.
 */
public class VOSearchReq {
    String orig_code ;
    String dest_code ;
    String date ;
    String adult_count ;
    String child_count ;
    String infant_count ;

    public String getOrig_code() {
        return orig_code;
    }

    public void setOrig_code(String orig_code) {
        this.orig_code = orig_code;
    }

    public String getDest_code() {
        return dest_code;
    }

    public void setDest_code(String dest_code) {
        this.dest_code = dest_code;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAdult_count() {
        return adult_count;
    }

    public void setAdult_count(String adult_count) {
        this.adult_count = adult_count;
    }

    public String getChild_count() {
        return child_count;
    }

    public void setChild_count(String child_count) {
        this.child_count = child_count;
    }

    public String getInfant_count() {
        return infant_count;
    }

    public void setInfant_count(String infant_count) {
        this.infant_count = infant_count;
    }
}
