package service.searchManagement;

import domain.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by monsieur maaz on 4/20/2017.
 */
@Path("/searchManagement")
public class searchManagement {

    public List<SearchRepVO> helperConverter (VOSearchResponses VOSResults){
        List<SearchRepVO> rv = new ArrayList<SearchRepVO>();
        convertor c = new convertor();
        for (VOSearchRep item:VOSResults.SearchResponse){
            for (int i = 0; i < item.seatClasses.size(); i++){
                rv.add(new SearchRepVO(item.flight, c.convert_airline_code(item.flight.getAirline_code()),
                        Integer.toString(item.flight.getFlight_no())
                        , c.convert_city(item.flight.getOrig_code()), c.convert_city(item.flight.getDest_code())
                        , c.convert_airplane_model(item.flight.getAirplane_model()), item.total_prices.get(i),
                         item.seatClasses.get(i), item.adult_cnt, item.child_cnt, item.inf_cnt)) ;
            }
        }
        return rv;
    }

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SearchRepVO> search(VOSearchReq VOSRQ) throws IOException, ClassNotFoundException {
        System.out.println("hello=========================================================");
        Akbarticket Akbar = new Akbarticket("178.62.207.47", 8081);
        VOSearchResponses VOSRP = Akbar.search(VOSRQ.getOrig_code(), VOSRQ.getDest_code(), VOSRQ.getDate(), VOSRQ.getAdult_count(),
                VOSRQ.getChild_count(), VOSRQ.getInfant_count());
        List<SearchRepVO> SRVs = helperConverter(VOSRP);
        System.out.println("we are fucked");
        return SRVs;
    }


}
