package service.searchManagement;

import domain.Flight;
import domain.SeatClass;

import java.util.ArrayList;

/**
 * Created by monsieur maaz on 4/23/2017.
 */
public class SearchRepVO {
    Flight flight;
    String airline_code;
    String flight_no;
    String orig_code;
    String dest_code;
    String airplane_model;

    int total_price ;
    SeatClass seatClass ;
    int adult_cnt;
    int child_cnt;
    int inf_cnt;

    public SearchRepVO(Flight flight, String airline_code, String flight_no, String orig_code, String dest_code, String airplane_model, int total_price, SeatClass seatClass, int adult_cnt, int child_cnt, int inf_cnt) {
        this.flight = flight;
        this.airline_code = airline_code;
        this.flight_no = flight_no;
        this.orig_code = orig_code;
        this.dest_code = dest_code;
        this.airplane_model = airplane_model;
        this.total_price = total_price;
        this.seatClass = seatClass;
        this.adult_cnt = adult_cnt;
        this.child_cnt = child_cnt;
        this.inf_cnt = inf_cnt;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public String getAirline_code() {
        return airline_code;
    }

    public void setAirline_code(String airline_code) {
        this.airline_code = airline_code;
    }

    public String getFlight_no() {
        return flight_no;
    }

    public void setFlight_no(String flight_no) {
        this.flight_no = flight_no;
    }

    public String getOrig_code() {
        return orig_code;
    }

    public void setOrig_code(String orig_code) {
        this.orig_code = orig_code;
    }

    public String getDest_code() {
        return dest_code;
    }

    public void setDest_code(String dest_code) {
        this.dest_code = dest_code;
    }

    public String getAirplane_model() {
        return airplane_model;
    }

    public void setAirplane_model(String airplane_model) {
        this.airplane_model = airplane_model;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public SeatClass getSeatClass() {
        return seatClass;
    }

    public void setSeatClass(SeatClass seatClass) {
        this.seatClass = seatClass;
    }

    public int getAdult_cnt() {
        return adult_cnt;
    }

    public void setAdult_cnt(int adult_cnt) {
        this.adult_cnt = adult_cnt;
    }

    public int getChild_cnt() {
        return child_cnt;
    }

    public void setChild_cnt(int child_cnt) {
        this.child_cnt = child_cnt;
    }

    public int getInf_cnt() {
        return inf_cnt;
    }

    public void setInf_cnt(int inf_cnt) {
        this.inf_cnt = inf_cnt;
    }
}
