package service.resultManagement;

import domain.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * Created by goodarzysepideh on 27/04/2017.
 */
@Path("/resultManagement")
public class resultManagement {
    @POST
    @Path("/result")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public VOResultRep result(VOResultReq VORR) throws IOException, ClassNotFoundException {
        Akbarticket Akbar = new Akbarticket("178.62.207.47", 8081);
        VOResultRep VOORRP = Akbar.temp_reserve(VORR);
        return VOORRP ;
    }
}
