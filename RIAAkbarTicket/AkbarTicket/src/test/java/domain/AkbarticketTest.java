package domain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class AkbarticketTest {
    private Akbarticket akbarticket;
    @Before
    public void setUp() {
        akbarticket = new Akbarticket();
    }
    @Test
    public void testScenario1() throws Exception{
        String from = "THR";
        String to = "MHD";
        String departureDate = "05Feb";
        String adultNum = "1";
        String childNum = "1";
        String infNum = "1";
        VOSearchResponses VOSRPS = akbarticket.search(from, to, departureDate, adultNum, childNum, infNum);
        assertEquals(VOSRPS.SearchResponse.size(), 1);
    }
    /*@Test
    public void testScenario2() throws Exception{
        VOTempResReq VOTRQ = new VOTempResReq();

        VOTRQ.orig_code = "THR";
        VOTRQ.dest_code = "MHD";
        VOTRQ.departure_time = "17:40";
        VOTRQ.arival_time = "18:40";
        VOTRQ.airplane_model = "M80";
        VOTRQ.date = "05Feb";
        VOTRQ.airline_code = "IR";
        VOTRQ.flight_no = "452";
        VOTRQ.seat_class = "Y";
        VOTRQ.adult_count = "1";
        VOTRQ.adult_price = "2000";
        VOTRQ.child_count = "1";
        VOTRQ.child_price = "2000";
        VOTRQ.infant_count = "1";
        VOTRQ.infant_price = "2000";
        VOTRQ.total_price = "6000";
        VOTRQ.num_of_seats = "8";
        Passenger p = new Passenger("mamad","mamadi","1234567890","female");
        Passenger p1 = new Passenger("mamad","mamadi","1234567890","female");
        Passenger p2 = new Passenger("mamad","mamadi","1234567890","female");
        ArrayList<Passenger> passengers = new ArrayList<Passenger>();
        passengers.add(p);
        passengers.add(p1);
        passengers.add(p2);
        VOTRQ.passengers = passengers;
        ReserveClass rc = akbarticket.reserve(VOTRQ);
        assertNotEquals(rc.getRef_code(), "");
    }*/
}