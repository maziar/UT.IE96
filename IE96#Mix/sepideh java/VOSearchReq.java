/**
 * Created by goodarzysepideh on 02/03/2017.
 */
public class VOSearchReq {
    public String orig_code ;
    public String dest_code ;
    public String date ;
    public String adult_count ;
    public String child_count ;
    public String infant_cout ;

    public VOSearchReq(String orig_code, String dest_code, String date, String adult_count, String child_count, String infant_cout) {
        this.orig_code = orig_code;
        this.dest_code = dest_code;
        this.date = date;
        this.adult_count = adult_count;
        this.child_count = child_count;
        this.infant_cout = infant_cout;
    }
}
