import java.util.ArrayList;

/**
 * Created by goodarzysepideh on 03/03/2017.
 */
public class VOTempResReq {
    public String orig_code ;
    public String dest_code ;
    public String date ;
    public String airline_code ;
    public String flight_no ;
    public String seat_class ;
    public String adult_count ;
    public String child_count ;
    public String infant_count ;
    public ArrayList<Passenger>passengers;
}
