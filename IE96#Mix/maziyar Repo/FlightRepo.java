import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by monsieur maaz on 3/2/2017.
 */
public class FlightRepo {
    private List<HashMap<Integer, Flight> > Flights;

    private static FlightRepo flight_repo = new FlightRepo();

    private FlightRepo () {
        Flights = new ArrayList<HashMap<Integer, Flight>>();
    }

    public static FlightRepo getFlight_repo() {return flight_repo;}

    public List<HashMap<Integer,Flight> > getFlights() { return Flights;}

    public void add_flight (Flight F, int id) {
        HashMap<Integer,Flight> temp = new HashMap<Integer, Flight>();
        temp.put(id , F);
        Flights.add(temp);
    }
}
