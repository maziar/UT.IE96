import java.util.ArrayList;
import java.util.List;

/**
 * Created by monsieur maaz on 3/2/2017.
 */
public class SeatClassRepo {
    private List<SeatClass> SeatClasses;

    private static SeatClassRepo seat_class_repo = new SeatClassRepo();

    private SeatClassRepo() {
        SeatClasses = new ArrayList<SeatClass>();
    }

    public static SeatClassRepo getSeat_class_repo() {
        return seat_class_repo;
    }

    public List<SeatClass> getSeatClasses() {
        return SeatClasses;
    }

    public void add_seat_class(SeatClass S) {
        SeatClasses.add(S);
    }
}
