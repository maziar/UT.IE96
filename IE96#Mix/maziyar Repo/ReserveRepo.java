import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by monsieur maaz on 3/2/2017.
 */
public class ReserveRepo {
    private List<Reserve> reserves;

    private static ReserveRepo reserveRepo = new ReserveRepo();

    private ReserveRepo () {
        reserves = new ArrayList<Reserve>();
    }

    public static ReserveRepo getReserveRepo() {return reserveRepo;}

    public List<Reserve> getReserves() { return reserves;}

    public void add_flight (Reserve R) {
        reserves.add(R);
    }
}
