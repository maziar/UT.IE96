function write_again(fo){
    document.getElementById("in_gray").innerHTML= "" ;
    document.getElementById("in_gray").innerHTML += '<div class="row padding in_gray_row"><div class="col-md-12 rtl titr"></div></div>' ;
    for(var i = 0 ; i < fo.length ; i++){
        var hidden = "" ;
        if(fo[i].style.display == "none"){
            hidden = 'style="display:none"' ;
        }
        var temp = '<form ' + hidden + ' method="post" action="controller.PerformRes.do" class="SearchResultFlights">' + fo[i].innerHTML + '</form>'
        in_gray.innerHTML += temp ;
        in_gray.innerHTML += '<div ' + hidden + ' class="row padding in_gray_row"><div class="col-md-12 rtl titr"></div></div>' ;
    }
}

function getSelectedIndex(sel){
    var t_fo = document.getElementsByTagName("form");
    var fo = [].slice.call(t_fo);
    for(var i = 0 ; i < fo.length ; i++){
        fo[i].style.display = "block" ;
    }
    var filterSelected = sel.options[sel.selectedIndex].value;
    if(filterSelected == "decPrice"){
        fo.sort(function( a , b){
            return parseInt(b.getElementsByClassName("total_price")[0].value) - parseInt(a.getElementsByClassName("total_price")[0].value);
        });
    } else if( filterSelected == "ascPrice") {
        fo.sort(function( a , b){
            return parseInt(a.getElementsByClassName("total_price")[0].value) - parseInt(b.getElementsByClassName("total_price")[0].value);
        });
    } else if( filterSelected == "airlineIR") {
        for(var i = 0 ; i < fo.length ; i++){
            if(fo[i].getElementsByClassName("airline_code")[0].value != "IR"){
                fo[i].style.display = "none";
            }
        }
    } else if( filterSelected == "airlineW5") {
        for(var i = 0 ; i < fo.length ; i++){
            if(fo[i].getElementsByClassName("airline_code")[0].value != "W5"){
                fo[i].style.display = "none";
            }
        }
    } else if( filterSelected == "seat_classY") {
        for(var i = 0 ; i < fo.length ; i++){
            if(fo[i].getElementsByClassName("seat_class")[0].value != "Y"){
                fo[i].style.display = "none";
            }
        }
    } else if( filterSelected == "seat_classB") {
        for(var i = 0 ; i < fo.length ; i++){
            if(fo[i].getElementsByClassName("seat_class")[0].value != "B"){
                fo[i].style.display = "none";
            }
        }
    } else if( filterSelected == "seat_classF") {
        for(var i = 0 ; i < fo.length ; i++){
            if(fo[i].getElementsByClassName("seat_class")[0].value != "F"){
                fo[i].style.display = "none";
            }
        }
    } else if( filterSelected == "seat_classM") {
        for(var i = 0 ; i < fo.length ; i++){
            if(fo[i].getElementsByClassName("seat_class")[0].value != "M"){
                fo[i].style.display = "none";
            }
        }
    } else if( filterSelected == "seat_classC") {
        for(var i = 0 ; i < fo.length ; i++){
            if(fo[i].getElementsByClassName("seat_class")[0].value != "C"){
                fo[i].style.display = "none";
            }
        }
    }
    write_again(fo);
}