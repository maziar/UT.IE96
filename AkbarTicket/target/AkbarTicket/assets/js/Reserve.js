function add_member(type){
    var num_of_seats = document.getElementById("num_of_seats").value;
    alert(parseInt(document.getElementById("count").value) + 1);
    alert(parseInt(num_of_seats));
    if(num_of_seats != "9" && parseInt(document.getElementById("count").value) + 1 > parseInt(num_of_seats) )
    {
        alert("این پرواز طرفیت اضافه کردن تعداد مسافران را ندارد!");
        return ;
    }
    var adult_count = document.getElementById("adult_count").value;
    var adult_price = document.getElementById("adult_price").value;
    var child_count = document.getElementById("child_count").value;
    var child_price = document.getElementById("child_price").value;
    var infant_count = document.getElementById("infant_count").value;
    var infant_price = document.getElementById("infant_price").value;
    var total_price = document.getElementById("total_price").value;
    if(type === 1){
        adult_count++;
        document.getElementById("adult_count").value = adult_count ;
        document.getElementById("adult_count_tbl").innerHTML = adult_count + " نفر ";
        document.getElementById("adult_sum_price_tbl").innerHTML = adult_count * adult_price + " ریال ";
        document.getElementById("total_price").value = parseInt(total_price) + parseInt(adult_price) ;
        document.getElementById("total_price_tbl").innerHTML = parseInt(total_price) + parseInt(adult_price) + " ریال ";
    }
    else if(type === 2){
        child_count ++ ;
        document.getElementById("child_count").value = child_count ;
        document.getElementById("child_count_tbl").innerHTML = child_count + " نفر ";
        document.getElementById("child_sum_price_tbl").innerHTML = child_count * child_price + " ریال ";
        document.getElementById("total_price").value = parseInt(total_price) + parseInt(child_price);
        document.getElementById("total_price_tbl").innerHTML = parseInt(total_price) + parseInt(child_price) + " ریال ";
    }
    else if(type === 3){
        infant_count++ ;
        document.getElementById("infant_count").value = infant_count ;
        document.getElementById("infant_count_tbl").innerHTML = infant_count + " نفر ";
        document.getElementById("infant_sum_price_tbl").innerHTML = infant_count * infant_price + " ریال ";
        document.getElementById("total_price").value = parseInt(total_price) + parseInt(infant_price) ;
        document.getElementById("total_price_tbl").innerHTML = parseInt(total_price) + parseInt(infant_price) + " ریال ";
    }
    document.getElementById("count").value = parseInt(document.getElementById("count").value) + 1 ;
    var form_passengers_part = document.getElementById("form_passengers_part");
    form_passengers_part.innerHTML = "" ;
    var count = 1 ;
    for (var i = 0 ; i < adult_count ; i++){
        form_passengers_part.innerHTML += '<div class="row padding in_gray_row form_row" id="bozorgsal"><div class="col-md-2 rtl form_label"><i class="rtl fa fa-male bozorgsal" aria-hidden="true"></i><span class="number">&nbsp;&nbsp;&nbsp;'+ count +' بزرگسال</span></div><div class="col-md-2 rtl"><select name="sex'+ count +'"><option value="MR" >آقای</option><option value="MRS" >خانم</option></select></div><div class="col-md-2 rtl"><input onclick="remove_class(this)" type="text" name="name' + count +'" placeholder="نام (انگلیسی)"/></div><div class="col-md-3 rtl"><input onclick="remove_class(this)" type="text" name="fname' + count + '" placeholder="نام خانوادگی (انگلیسی)"/></div><div class="col-md-3 rtl"><input onclick="remove_class(this)" type="text" name="internationalID'+ count +'" placeholder="شماره ملی"></div></div>' ;
        count++ ;
    }
    for(var i = 0 ; i < child_count ; i++){
        form_passengers_part.innerHTML += '<div class="row padding in_gray_row form_row" id = "khordsal"><div class="col-md-2 rtl form_label"><i class="rtl fa fa-child bozorgsal" aria-hidden="true"></i><span class="number">&nbsp;&nbsp;&nbsp;'+ count +' خردسال</span></div><div class="col-md-2 rtl"><select name="sex'+ count +'"<option value="MR" >آقای</option><option value="MRS" >خانم</option></select></div><div class="col-md-2 rtl"><input onclick="remove_class(this)" type="text" name="name'+ count +'" placeholder="نام (انگلیسی)"/></div><div class="col-md-3 rtl"><input onclick="remove_class(this)" type="text" name="fname'+ count +'" placeholder="نام خانوادگی (انگلیسی)"/></div><div class="col-md-3 rtl"><input onclick="remove_class(this)" type="text" name="internationalID'+ count +'" placeholder="شماره ملی"></div></div>' ;
        count++ ;
    }
    for(var i = 0 ; i < infant_count ; i++){
        form_passengers_part.innerHTML += '<div class="row padding in_gray_row form_row"><div class="col-md-2 rtl form_label"><i class="rtl fa fa-child nozad" aria-hidden="true"></i><span class="number">&nbsp;&nbsp;&nbsp;'+ count +' نوزاد</span></div><div class="col-md-2 rtl"><select name="sex'+ count +'"><option value="MR">آقای</option><option value="MRS">خانم</option></select></div><div class="col-md-2 rtl"><input onclick="remove_class(this)" name="name'+ count +'" placeholder="نام (انگلیسی)" type="text"></div><div class="col-md-3 rtl"><input onclick="remove_class(this)" name="fname'+ count +'" placeholder="نام خانوادگی (انگلیسی)" type="text"></div><div class="col-md-3 rtl"><input onclick="remove_class(this)" name="internationalID'+ count +'" placeholder="شماره ملی" type="text"></div></div>' ;
        count++ ;
    }

}

function add_class(element , t_class_name){
    var class_name = element.className ;
    if(!class_name.includes(t_class_name)){
        var t_t_class_name = " " + t_class_name ;
        element.className += t_t_class_name ;
    }
}

function remove_class(element){
    var class_name = element.className ;
    element.className = class_name.replace("error", "");
}

class checker {

    constructor() {
        //alert("created");
    }

    name_checking(i){
        var name = document.getElementsByName("name" + i)[0].value;
        if(name == "" || name.length <= 2 || !(name.search(/[^a-zA-Z]+/) === -1)){
            this.error = 1 ;
            add_class(document.getElementsByName("name" + i)[0] , "error");
        }
    }

    names_checking(){
        var count = document.getElementById("count").value ;
        for(var i = 1 ; i <= count ; i++){
            this.name_checking(i);
        }
    }

    fname_checking(i){
        var fname = document.getElementsByName("fname" + i)[0].value;
        if(fname == "" || fname.length <= 2 || !(fname.search(/[^a-zA-Z]+/) === -1)){
            this.error = 1 ;
            add_class(document.getElementsByName("fname" + i)[0] , "error");

        }
    }

    fnames_checking(){
        var count = document.getElementById("count").value ;
        for(var i = 1 ; i <= count ; i++){
            this.fname_checking(i);
        }
    }

    internationalID_checking(i){
        var internationalID = document.getElementsByName("internationalID" + i)[0].value;
        var reg = /^\d+$/ ;
        if(internationalID == "" || internationalID.length != 10 || !reg.test(internationalID) ){
            this.error = 1 ;
            add_class(document.getElementsByName("internationalID" + i)[0] , "error");
        }
    }

    internationalIDs_checking(){
        var count = document.getElementById("count").value ;
        for(var i = 1 ; i <= count ; i++){
            this.internationalID_checking(i);
        }
    }

    validate(){
        this.error = 0 ;
        this.names_checking();
        this.fnames_checking();
        this.internationalIDs_checking();
        return this.error ;
    }    

};

function checking_initiator(){
    let checker_ins = new checker();
    let error = checker_ins.validate();
    if(!error) {
        document.getElementById("ReserveForm").submit();
    }
}