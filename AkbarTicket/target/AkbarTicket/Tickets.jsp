<%@page import="domain.* , java.util.*"%>
<%@ page import="jdk.nashorn.internal.ir.RuntimeNode" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>

<!doctype html>
<html>
    <head>
        <title>اکبر تیکت</title>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" type="text/css" href="assets/css/Tickets.css">
		<link rel="stylesheet" type="text/css" href="assets/css/cssreset.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
		<div class="header">
            
            <div class="companySet">
				<img id="logo" alt="" src="assets/LogoBlack.png">
                <span class="company">اکبر تیکت</span>
				
                    <i class="fa fa-registered" id="sideCompany" aria-hidden="true"></i>
				
		    </div>
			<div class="user" >
				<i class="fa fa-user" aria-hidden="true" id="user-icon"></i>
				<span id="username">
					نام کاربر
				</span>
				<ul>
					<li>
						بلیطهای من
					</li>
					<li>
						خروج
					</li>
				</ul>
			</div>
    	</div>
		<div class="container">
			<div class="containerHeader">
				
					<span>جستجوی پرواز<span><i  class="fa fa-chevron-left left_arrow" aria-hidden="true"></i></span></span>
					<span>انتخاب پرواز<span><i  class="fa fa-chevron-left left_arrow" aria-hidden="true"></i></span></span>
					<span>ورود اطلاعات<span><i  class="fa fa-chevron-left left_arrow" aria-hidden="true"></i></span></span>
					<span class="selected">دریافت بلیط</span>
				
			</div>
			<div class="container_body">
				<div class="title">
					<span class="TicketsHeader">بلیط‌های صادر شده</span>
					<a href="#modal"><button class="print">چاپ همه</button></a>
                    <div id="modal" class="overlay">
                        <div class="popup">
                            <a class="close" href="#">&times;</a>
                            <div class="content">
                                <button>PDF</button>
                                <button>ایمیل</button>
                            </div>
                        </div>
                    </div>
				
				</div>
                <%
                  int count = Integer.parseInt(request.getParameter("count")) ;
                  for(int i = 0 ; i < count ; i++){

                %>
				<div class="ticket">
					<div class="num_codes">
						<span class="VA color_set">شماره بلیط</span>
						<span class="numbers"><%= ((ReserveClass)request.getAttribute("final_res")).getPassengers().get(i).getTicket().getTicket_no()%></span>
						<span class="VA color_set">کد مرجع</span>
						<span class="numbers"><%= ((ReserveClass)request.getAttribute("final_res")).getRef_code()%></span>
					</div>
					<hr class="color_set2" />
					<div class="t_info">
						<span class="color_set name_margin">نام</span>
						<span id="Ltr"><%= ((ReserveClass)request.getAttribute("final_res")).getPassengers().get(i).getSex() %> <%= ((ReserveClass)request.getAttribute("final_res")).getPassengers().get(i).getFirstName() %> <%= ((ReserveClass)request.getAttribute("final_res")).getPassengers().get(i).getSurName() %></span>
						<span class="color_set flight_margin">پرواز</span>
						<span class="font_set"><%= request.getParameter("flight_no") %> <%= ((VOConverted)request.getAttribute("converted")).getAirline_code() %></span>
						<span class="eco_class"><%= ((VOConverted)request.getAttribute("converted")).getSeat_class() %></span>
						<span><%= ((VOConverted)request.getAttribute("converted")).getAirplane_model() %></span>
					</div>
					<hr class="color_set2" />
					<div class="date_time">
						<div class="inf_titles">
							<div class="infobox"><div class="infh">از</div><div class="output"><%= ((VOConverted)request.getAttribute("converted")).getOrig_city() %></div></div>
							<div class="infobox"><div class="infh">به</div><div class="output"><%= ((VOConverted)request.getAttribute("converted")).getDest_city() %></div></div>
							<div class="infobox"><div class="infh">تاریخ</div><div class="outputDate"><%= ((VOConverted)request.getAttribute("converted")).getDate() %></div></div>
							<div class="infobox"><div class="infh">خروج</div><div class="output"><%= ((VOConverted)request.getAttribute("converted")).getDeparture_time() %></div></div>
							<div class="infobox"><div class="infh">ورود</div><div class="output"><%= ((VOConverted)request.getAttribute("converted")).getArrival_time() %></div></div>
							
						</div>
					</div>
				</div>
                <%
                   }
                %>
            </div>
		</div>
		<div class="footer colot_set">
			مازیار نظری ، سپیده گودرزی | دانشکده‌ی فنی دانشگاه تهران، بهار 1396
		</div>
    </body>
</html>
