package controller;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PerformSearchTest {

    private PerformSearch performSearch;

    @Before
    public void setUp() throws Exception {
        performSearch = new PerformSearch();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testDoPost() throws Exception {
        HttpServletRequestMock HSRQM = new HttpServletRequestMock();
        HttpServletResponseMock HSRPM = new HttpServletResponseMock();

        HSRQM.addParameter("from", "THR");
        HSRQM.addParameter("to", "MHD");
        HSRQM.addParameter("departureDate", "05Feb");
        HSRQM.addParameter("arrivalDate", "05Feb");
        HSRQM.addParameter("adultNum", "1");
        HSRQM.addParameter("childNum", "1");
        HSRQM.addParameter("infNum", "1");
        performSearch.doPost(HSRQM, HSRPM);
        String res = HSRPM.getData().toString();
        org.jsoup.nodes.Document resp = Jsoup.parse(res);
        Elements ntmDivs = resp.getElementsByClass("myform");
        int numOFforms = ntmDivs.size();
        assertEquals(3, numOFforms);
    }

    @Test
    public void testDoPost2() throws Exception {
        HttpServletRequestMock HSRQM = new HttpServletRequestMock();
        HttpServletResponseMock HSRPM = new HttpServletResponseMock();

        HSRQM.addParameter("from", "MHD");
        HSRQM.addParameter("to", "THR");
        HSRQM.addParameter("departureDate", "05Feb");
        HSRQM.addParameter("arrivalDate", "05Feb");
        HSRQM.addParameter("adultNum", "10");
        HSRQM.addParameter("childNum", "10");
        HSRQM.addParameter("infNum", "10");
        performSearch.doPost(HSRQM, HSRPM);
        String res = HSRPM.getData().toString();
        org.jsoup.nodes.Document resp = Jsoup.parse(res);
        Elements ntmDivs = resp.getElementsByClass("myform");
        int numOFforms = ntmDivs.size();
        assertEquals(0, numOFforms);
    }

    @Test
    public void testDoPost3() throws Exception {
        HttpServletRequestMock HSRQM = new HttpServletRequestMock();
        HttpServletResponseMock HSRPM = new HttpServletResponseMock();

        HSRQM.addParameter("from", "MHD");
        HSRQM.addParameter("to", "THR");
        HSRQM.addParameter("departureDate", "06Feb");
        HSRQM.addParameter("arrivalDate", "06Feb");
        HSRQM.addParameter("adultNum", "10");
        HSRQM.addParameter("childNum", "10");
        HSRQM.addParameter("infNum", "10");
        performSearch.doPost(HSRQM, HSRPM);
        String res = HSRPM.getData().toString();
        org.jsoup.nodes.Document resp = Jsoup.parse(res);
        Elements ntmDivs = resp.getElementsByClass("myform");
        int numOFforms = ntmDivs.size();
        assertEquals(1, numOFforms);
    }

    @Test
    public void testDoPost4() throws Exception {
        HttpServletRequestMock HSRQM = new HttpServletRequestMock();
        HttpServletResponseMock HSRPM = new HttpServletResponseMock();

        HSRQM.addParameter("from", "THR");
        HSRQM.addParameter("to", "IFN");
        HSRQM.addParameter("departureDate", "07Feb");
        HSRQM.addParameter("arrivalDate", "07Feb");
        HSRQM.addParameter("adultNum", "10");
        HSRQM.addParameter("childNum", "10");
        HSRQM.addParameter("infNum", "10");
        performSearch.doPost(HSRQM, HSRPM);
        String res = HSRPM.getData().toString();
        org.jsoup.nodes.Document resp = Jsoup.parse(res);
        Elements ntmDivs = resp.getElementsByClass("myform");
        int numOFforms = ntmDivs.size();
        assertEquals(0, numOFforms);
    }
}