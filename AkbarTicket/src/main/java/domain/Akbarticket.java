package domain;

import domain.ReserveClass;

import java.io.IOException;

/**
 * Created by goodarzysepideh on 02/03/2017.
 */
public class Akbarticket {

    private ServerProvider sp ;

    public Akbarticket( String apa_ip , int apa_port_no) throws IOException{
        sp = new ServerProvider(apa_ip , apa_port_no);
    }

    public void run(){
        System.out.println("hello");
    }

    public VOSearchResponses search(String from, String to, String depDate, String adultNum, String childNum, String infNum) throws IOException, ClassNotFoundException{
        VOSearchReq VOS = new VOSearchReq(from, to, depDate, adultNum, childNum, infNum);
        VOSearchResponses VOP = new VOSearchResponses();
        try {
            VOP = sp.search(VOS);
        }catch(IOException ex) {
            //some error handling
        }
        finally {
            System.out.println("[dbg] Value object of search Response: "  + VOP.SearchResponse.get(0).flight.getAirline_code());
            return VOP;
        }
    }

    public ReserveClass reserve(VOTempResReq votrr)throws IOException{
        VOTempResRep res = sp.TemporalReserve(votrr);
        VOFinResReq req = new VOFinResReq();
        req.token = res.token ;
        VOFinResRep final_res = sp.FinalReserve(req);
        return final_res.reserve ;
    }

}
