package domain;

import domain.VOConverted;

/**
 * Created by goodarzysepideh on 06/03/2017.
 */
public class convertor {

    public VOConverted convert(String tairline_code , String torig_city, String tdest_city , String tairplane_model , String tseat_class , String tdate , String tdepartue_time , String tarrival_time){
        String airline_code = convert_airline_code(tairline_code);
        String orig_city = convert_city(torig_city);
        String dest_city = convert_city(tdest_city);
        String airplane_model = convert_airplane_model(tairplane_model);
        String seat_class = convert_seat_class(tseat_class);
        String date = convert_date(tdate);
        String departure_time = convert_time(tdepartue_time);
        String arrival_time = convert_time(tarrival_time);
        VOConverted res = new VOConverted(airline_code , orig_city , dest_city , airplane_model , seat_class , date , departure_time , arrival_time);
        return res;
    }

    public String convert_airline_code(String airline_code){
        String res = airline_code ;
        if(airline_code.equals("IR")){
            res = "ایران ایر";
        }
        else if(airline_code.equals("W5")){
            res= "ماهان";
        }
        return res;
    }

    public String convert_city(String city){
        String res = "" ;
        if(city.equals("THR")){
            res = "تهران";
        }
        else if(city.equals("MHD")){
            res= "مشهد";
        }
        else if(city.equals("IFN")){
            res= "اصفهان";
        }
        return res;
    }

    public String convert_airplane_model(String airplane_model){
        String res = airplane_model ;
        if(airplane_model.equals("M80")){
            res = "بوئینگ MD83";
        }
        else if(airplane_model.equals("351")){
            res = "ایرباس A320";
        }
        return res ;
    }

    public String convert_seat_class(String seat_class){
        String res = seat_class ;
        if(seat_class.equals("Y")){
            res = "کلاس اقتصادی";
        }
        return res ;
    }

    String convert_date(String date){
        String res = "" ;
        res = date ;
        return res ;
    }

    public String convert_time(String time){
        String res = "" ;
        res = time.substring(0,2)+ ":" + time.substring(2,4);
        return res ;
    }
}
