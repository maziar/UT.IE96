package domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by monsieur maaz on 3/5/2017.
 */
public class VOSearchResponses {
    public List<VOSearchRep> SearchResponse;

    public VOSearchResponses() {
        SearchResponse = new ArrayList<VOSearchRep>();
    }

    public void addVOSearchResponses(VOSearchRep VOSin) {
        SearchResponse.add(VOSin);
    }
}
