package domain;

/**
 * Created by goodarzysepideh on 07/03/2017.
 */
public class VOConverted {
    public String airline_code ;
    public String orig_city ;
    public String dest_city ;
    public String airplane_model ;
    public String seat_class ;
    public String date;
    public String departure_time ;
    public String arrival_time ;

    public VOConverted(String airline_code, String orig_city, String dest_city, String airplane_model, String seat_class, String date, String departure_time, String arrival_time) {
        this.airline_code = airline_code;
        this.orig_city = orig_city;
        this.dest_city = dest_city;
        this.airplane_model = airplane_model;
        this.seat_class = seat_class;
        this.date = date;
        this.departure_time = departure_time;
        this.arrival_time = arrival_time;
    }

    public String getAirline_code() {
        return airline_code;
    }

    public String getOrig_city() {
        return orig_city;
    }

    public String getDest_city() {
        return dest_city;
    }

    public String getAirplane_model() {
        return airplane_model;
    }

    public String getDate() {
        return date;
    }

    public String getSeat_class() {
        return seat_class;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public String getArrival_time() {
        return arrival_time;
    }
}
