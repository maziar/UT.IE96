package domain;

import java.util.ArrayList;

/**
 * Created by monsieur maaz on 2/9/2017.
 */
public class ReserveClass {
    private int flight ;
    private SeatClass seat_class ;
    private int adult_cnt;
    private int child_cnt;
    private int infant_cnt;
    private ArrayList<Passenger> passengers;
    private String token;
    private int total_price ;
    private String Ref_code ;

    public ReserveClass(int flight, SeatClass seat_class, int adult_cnt, int child_cnt, int infant_cnt) {
        this.flight = flight;
        this.seat_class = seat_class;
        this.adult_cnt = adult_cnt;
        this.child_cnt = child_cnt;
        this.infant_cnt = infant_cnt;
        passengers = new ArrayList<Passenger>();
    }

    public void add_passenger(Passenger p) {
        passengers.add(p);
        System.out.println("[dbg] here we add this passenger !");
    }

    public Flight getFlight() {
        Flight f = FlightRepo.getFlight_repo().getFlights().get(flight);
        return f;
    }

    public SeatClass getSeat_class() {
        return seat_class;
    }

    public int getAdult_cnt() {
        return adult_cnt;
    }

    public int getChild_cnt() {
        return child_cnt;
    }

    public int getInfant_cnt() {
        return infant_cnt;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public String get_list_of_passengers(){
        String result = "";
        for (int i = 0; i < passengers.size(); i++) {
            result += (passengers.get(i).get_passenger_info());
        }
        return result;
    }

    public ArrayList<Passenger> getPassengers() {
        return passengers;
    }

    public String get_info(){
        System.out.println(Integer.toString(flight));
        return this.getFlight().get_filght_info() + " " + this.getSeat_class().getSeat_class() + " " + this.getAdult_cnt() + " " + this.getChild_cnt() + " " + this.getInfant_cnt();
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void set_pass_ticktNo(int k, Ticket ticket){
        this.passengers.get(k).setTicket(ticket);
    }

    public void setRef_code(String ref_code) {
        Ref_code = ref_code;
    }

    public String getRef_code() {
        return Ref_code;
    }

    public String get_final_res_info(){
        String finalized_res = "" ;
        for(int k = 0 ; k < passengers.size() ; k++){
            String first_name = passengers.get(k).getFirstName();
            String sur_name = passengers.get(k).getSurName();
            String temp = first_name +  " " + sur_name + "  " + getRef_code() + " " + passengers.get(k).getTicket().getTicket_no() +
                    " " + getFlight().getOrig_code() + " " + getFlight().getDest_code() + " " + getFlight().getAirline_code() + " "
                    + getFlight().getFilght_no() + " " + getSeat_class().getSeat_class() + " " + getFlight().getDeparture_time() + " " +
                    getFlight().getArrival_time() + " " + getFlight().getAirplane_model() ;
            if ( k != passengers.size() - 1){
                temp += "\n" ;
            }
            finalized_res += temp ;
        }
        return finalized_res ;
    }

    public void set_flight_additional_info(String departure_time , String arrival_time , String airplane_model){
        FlightRepo.getFlight_repo().setDeparture_time(flight, departure_time);
        FlightRepo.getFlight_repo().setArrival_time(flight, arrival_time);
        FlightRepo.getFlight_repo().setAirplane_model(flight, airplane_model);
    }

    public void set_seat_class(int adult_price , int child_price , int infant_price){
        this.seat_class.setAdult_price(adult_price);
        this.seat_class.setChild_price(child_price);
        this.seat_class.setInfant_price(infant_price);
    }
}
