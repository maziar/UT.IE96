package domain;

/**
 * Created by monsieur maaz on 2/9/2017.
 */
public class Passenger {
    private String FirstName;
    private String SurName;
    private String NationalID;
    private String sex;
    private Ticket ticket;

    public Passenger(String fn, String sn, String ni, String sx){
        FirstName = fn;
        SurName = sn;
        NationalID = ni;
        sex = sx ;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getSurName() {
        return SurName;
    }

    public String getNationalID() {
        return NationalID;
    }

    public String getSex() {
        return sex;
    }

    String get_passenger_info() {
        return FirstName + " " + SurName + " " + NationalID + "\n";
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Ticket getTicket() {
        return ticket;
    }
}
