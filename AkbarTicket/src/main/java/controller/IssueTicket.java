package controller;

import domain.*;
import org.apache.log4j.Logger;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by goodarzysepideh on 10/03/2017.
 */
public class IssueTicket extends HttpServlet {
    private static final Logger logger = Logger.getLogger(IssueTicket.class);
    public void init() throws ServletException {

    }
    public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        Akbarticket at = new Akbarticket("188.166.78.119", 8081);
        VOTempResReq votrr = new VOTempResReq();
        votrr.orig_code = request.getParameter("orig_code");
        votrr.dest_code = request.getParameter("dest_code");
        votrr.date = request.getParameter("date");
        votrr.airline_code = request.getParameter("airline_code");
        votrr.flight_no = request.getParameter("flight_no");
        votrr.seat_class = request.getParameter("seat_class");
        votrr.adult_count = request.getParameter("adult_count");
        votrr.child_count = request.getParameter("child_count");
        votrr.infant_count = request.getParameter("infant_count");
        votrr.passengers = new ArrayList<Passenger>();
        int count = Integer.parseInt(request.getParameter("count"));
        for(int i = 0 ; i < count ; i++){
            Passenger p = new Passenger(
                    request.getParameter("name" + Integer.toString(i + 1)),
                    request.getParameter("fname" + Integer.toString(i + 1)),
                    request.getParameter("internationalID" + Integer.toString(i + 1)),
                    request.getParameter("sex" + Integer.toString(i + 1))
            );
            votrr.passengers.add(p);
        }
        ReserveClass final_res = at.reserve(votrr);
        System.out.println("we are hear");
        System.out.println("1.RES" + votrr.flight_no);
        System.out.println("2.#" + votrr.orig_code);
        System.out.println("3.#" + votrr.dest_code);
        System.out.println("4.#" + votrr.date);
        System.out.println("5.#" + final_res.getToken());
        System.out.println("6.#" + votrr.adult_count);
        System.out.println("7.#" + votrr.child_count);
        System.out.println("#" + votrr.infant_count);
        logger.info("RES" + votrr.flight_no + "#" + votrr.orig_code + "#" + votrr.dest_code + "#" + votrr.date + " " + final_res.getToken() + " " + votrr.adult_count + " " + votrr.child_count + " " + votrr.infant_count);
         convertor conv =  new convertor();
        VOConverted voc = conv.convert(request.getParameter("airline_code") ,
                request.getParameter("orig_code") ,
                request.getParameter("dest_code") ,
                request.getParameter("airplane_model") ,
                request.getParameter("seat_class") ,
                request.getParameter("date"),
                request.getParameter("departure_time"),
                request.getParameter("arrival_time")
        );
        request.setAttribute("converted" , voc);
        request.setAttribute("final_res" , final_res);
        //logger.info("FINRES" + final_res.getToken() + " " + final_res.getRef_code() + " " + final_res.getTotal_price());
        request.getRequestDispatcher("Tickets.jsp").forward(request, response);

    }

    public void destroy() {
    }
}