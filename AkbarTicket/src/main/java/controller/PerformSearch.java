package controller;

import domain.Akbarticket;
import domain.VOSearchRep;
import domain.VOSearchResponses;
import domain.convertor;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by monsieur maaz on 3/3/2017.
 */
public class PerformSearch extends HttpServlet {
    private static final Logger logger = Logger.getLogger(PerformSearch.class);
    public void init() throws ServletException{

    }
    public void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        doPost(request, response);
    }


    public void result_page(PrintWriter pw, VOSearchResponses VOSin) throws IOException{
        convertor c = new convertor();
        ////////////////////////////////page
        pw.println("<!DOCTYPE html>");
        pw.println("<html>");
        pw.println("    <head>");
        pw.println("        <title>اکبر تیکت</title>");
        pw.println("        <meta charset=\"utf-8\">");
        pw.println("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
        pw.println("        <!-- Latest compiled and minified CSS -->");
        pw.println("        <link rel=\"stylesheet\" type=\"text/css\" href=\"assets/css/bootstrap/css/bootstrap.css\">");
        pw.println("        <link rel=\"stylesheet\" type=\"text/css\" href=\"assets/css/cssreset.css\">");
        pw.println("        <!--<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>");
        pw.println("        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>-->");
        pw.println("        <!-- font awesome -->");
        pw.println("        <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\">");
        pw.println("        <link rel=\"stylesheet\" type=\"text/css\" href=\"assets/css/Results.css\">");
        pw.println("        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.18.1/babel.min.js\"></script>");
        pw.println("    </head>");
        pw.println("    <body>");
        pw.println("        <div class=\"row my_nav\">");
        pw.println("            <div class=\"col-md-2 rtl\">");
        pw.println("            </div>");
        pw.println("            <div class=\"col-md-4 col-xs-6 rtl\">");
        pw.println("                    <span class=\"logo rtl\">");
        pw.println("                        <img alt=\"logo\" class=\"rtl\" src=\"assets/img/LogoBlack.png\"/>");
        pw.println("                        <span class=\"rtl hidden-xs\">&nbsp;&nbsp;اکبر تیکت&nbsp;</span>");
        pw.println("                        <i class=\"fa fa-registered\"></i>");
        pw.println("                    </span>");
        pw.println("            </div>");
        pw.println("            <div class=\"karbar col-md-4 col-xs-6 rtl\">");
        pw.println("                <div class=\"row\">");
        pw.println("                    <div class=\"col-md-12\">");
        pw.println("                        <i class=\"fa fa-user rtl\"></i>");
        pw.println("                        <span class=\"rtl hidden-xs\">&nbsp;&nbsp;&nbsp;نام کاربر</span>");
        pw.println("                    </div>");
        pw.println("                </div>");
        pw.println("                <div class=\"row my_drop-down\">");
        pw.println("                    <div class=\"col-md-4 rtl\">");
        pw.println("                        <ul>");
        pw.println("                            <li>");
        pw.println("                              بلیطهای من");
        pw.println("                            </li>");
        pw.println("                            <li>");
        pw.println("                              خروج");
        pw.println("                            </li>");
        pw.println("                        </ul>");
        pw.println("                    </div>");
        pw.println("                    <div class=\"col-md-8 rtl\">");
        pw.println("                    </div>");
        pw.println("                </div>");
        pw.println("            </div>");
        pw.println("            <div class=\"col-md-2 rtl\">");
        pw.println("            </div>");
        pw.println("        </div>");
        pw.println("        <div id=\"container\" class=\"container-fluid my-container\">");
        pw.println("            <div class=\"row\">");
        pw.println("                <div class=\"col-md-12\" id=\"blue_menu\">");
        pw.println("                    <div class=\"row\">");
        pw.println("                        <div class=\"col-md-3\">");
        pw.println("                        </div>");
        pw.println("                        <div class=\"col-md-6\">");
        pw.println("                            <div class=\"row blue_menu_items\">");
        pw.println("                                <div class=\"rtl col-md-3 blue_menu_item hidden-xs\">");
        pw.println("                                    <span class=\"rtl center\">جستجوی پرواز&nbsp;&nbsp;&nbsp;</span>");
        pw.println("                                    <i class=\"rtl fa fa-chevron-left\" aria-hidden=\"true\"></i>");
        pw.println("                                </div>");
        pw.println("                                <div class=\"rtl col-md-3 blue_menu_item selected\">");
        pw.println("                                    <span class=\"rtl center\">انتخاب پرواز&nbsp;&nbsp;&nbsp;</span>");
        pw.println("                                    <i class=\"rtl fa fa-chevron-left\" aria-hidden=\"true\"></i>");
        pw.println("                                </div>");
        pw.println("                                <div class=\"rtl col-md-3 blue_menu_item hidden-xs\">");
        pw.println("                                    <span class=\"rtl center\">ورود اطلاعات&nbsp;&nbsp;&nbsp;</span>");
        pw.println("                                    <i class=\"rtl fa fa-chevron-left\" aria-hidden=\"true\"></i>");
        pw.println("                                </div>");
        pw.println("                                <div class=\"rtl col-md-3 blue_menu_item\">");
        pw.println("                                    <span class=\"rtl center hidden-xs\">دریافت بلیط</span>");
        pw.println("                                    <i class=\"rtl\">&nbsp;&nbsp;&nbsp;</i>");
        pw.println("                                </div>");
        pw.println("                            </div>");
        pw.println("                        </div>");
        pw.println("                        <div class=\"col-md-3\">");
        pw.println("                        </div>");
        pw.println("                    </div>");
        pw.println("                </div>");
        pw.println("            </div>");
        pw.println("            <div class=\"row\">");
        pw.println("                <div class=\"col-md-2\">");
        pw.println("                </div>");
        pw.println("                <div class=\"col-md-8\">");
        pw.println("                    <div class=\"row\" id=\"under_menu\">");
        pw.println("                        <div class=\"col-md-3 rtl\">");
        pw.println("                            <i class=\"rtl font-awwsome fa fa-arrow-right\" aria-hidden=\"true\" ></i>");
        pw.println("                            <a href=\"index.jsp\"><span class=\"rtl\">&nbsp;&nbsp;برگشت به صفحه جستجو</span></a>");
        pw.println("                        </div>");
        pw.println("                        <div class=\"col-md-5 rtl\">");
        pw.println("                            <select class=\"filter\" id=\"filterRes\" onchange=\"getSelectedIndex(this);\">");
        pw.println("                                <option value=\"decPrice\">قیمت نزولی</option>");
        pw.println("                                <option value=\"ascPrice\">قیمت صعودی</option>");
        pw.println("                                <option value=\"airlineIR\">هواپیمایی ایران ایر</option>");
        pw.println("                                <option value=\"airlineW5\">هواپیمایی ماهان</option>");
        pw.println("                                <option value=\"seat_classY\">کلاس اقتصادی</option>");
        pw.println("                                <option value=\"seat_classF\">کلاس برتر</option>");
        pw.println("                                <option value=\"seat_classB\">کلاس تجاری</option>");
        pw.println("                                <option value=\"seat_classM\">Mclass</option>");
        pw.println("                                <option value=\"seat_classC\">Cclass</option>");
        pw.println("                            </select>");
        pw.println("                        </div>");
        pw.println("                        <div class=\"col-md-4 rtl\">");
        int count = 0;
        for(VOSearchRep searchBean : VOSin.SearchResponse) {
            for (int i = 0; i < searchBean.seatClasses.size(); i++) {
                count++;
            }
        }
        pw.println("                            <span class=\"rtl number\">" + count +" پرواز با مشخصات دلخواه شما پیدا شد</span>");
        pw.println("                        </div>");
        pw.println("                    </div>");
        //this
        pw.println("                    <div class=\"row curve gray\">");
        pw.println("                        <div class=\"col-md-12\" id=\"in_gray\">");
        pw.println("                            <div class=\"row padding in_gray_row\">");
        pw.println("                                <div class=\"col-md-12 rtl titr\">");
        pw.println("                                </div>");
        pw.println("                            </div>");
        for(VOSearchRep searchBean : VOSin.SearchResponse){
            for (int i = 0; i < searchBean.seatClasses.size(); i++) {

                pw.println("							<form class=\"SearchResultFlights\" method=\"post\" action=\"controller.PerformRes.do\">");
                pw.println("							<input type=\"hidden\" class=\"airline_code\" name=\"airline_code\" value=\""+ searchBean.flight.getAirline_code()+"\">");
                pw.println("							<input type=\"hidden\" name=\"flight_no\" value=\""+searchBean.flight.getFilght_no()+"\">");
                pw.println("							<input type=\"hidden\" name=\"date\" value=\""+searchBean.flight.getDate()+"\">");
                pw.println("							<input type=\"hidden\" name=\"orig_code\" value=\""+searchBean.flight.getOrig_code()+"\">");
                pw.println("							<input type=\"hidden\" name=\"dest_code\" value=\""+searchBean.flight.getDest_code()+"\">");
                pw.println("							<input type=\"hidden\" name=\"departure_time\" value=\""+searchBean.flight.getDeparture_time().substring(0,2) +searchBean.flight.getDeparture_time().substring(3) +"\">");
                pw.println("							<input type=\"hidden\" name=\"arrival_time\" value=\""+searchBean.flight.getArrival_time().substring(0,2)+searchBean.flight.getArrival_time().substring(3)+"\">");
                pw.println("							<input type=\"hidden\" name=\"airplane_model\" value=\""+searchBean.flight.getAirplane_model()+"\">");
                pw.println("							<input type=\"hidden\" class=\"seat_class\" name=\"seat_class\" value=\""+searchBean.seatClasses.get(i).getSeat_class()+"\">");
                pw.println("							<input type=\"hidden\" name=\"adult_count\" value=\""+searchBean.adult_cnt+"\">");
                pw.println("							<input type=\"hidden\" name=\"adult_price\" value=\""+searchBean.seatClasses.get(i).getAdult_price()+"\">");
                pw.println("							<input type=\"hidden\" name=\"child_count\" value=\""+searchBean.child_cnt+"\">");
                pw.println("							<input type=\"hidden\" name=\"child_price\" value=\""+searchBean.seatClasses.get(i).getChild_price()+"\">");
                pw.println("							<input type=\"hidden\" name=\"infant_count\" value=\""+searchBean.inf_cnt+"\">");
                pw.println("							<input type=\"hidden\" name=\"infant_price\" value=\""+searchBean.seatClasses.get(i).getInfant_price()+"\">");
                pw.println("							<input type=\"hidden\" class=\"total_price\" name=\"total_price\" value=\""+searchBean.total_prices.get(i)+"\">");
                pw.println("                            <input type=\"hidden\" class=\"num_of_seats\" name=\"num_of_seats\" value=\"" + searchBean.seatClasses.get(i).getCount()+"\">");
                pw.println("                            <div class=\"row in_gray_row white\" id=\"t_info\">");
                pw.println("                                <div class=\"col-md-3 rtl\">");
                pw.println("                                    <div class=\"row\">");
                pw.println("                                        <div class=\"col-md-12 rtl in_white_row number\">");
                pw.println(c.convert_airline_code(searchBean.flight.getAirline_code()) + searchBean.flight.getFilght_no());
                pw.println("                                        </div>");
                pw.println("                                        <div class=\"col-md-12 rtl in_white_row\">");
                pw.println("                                            <i class=\"rtl t_font_awwsome fa fa-calendar-o\" aria-hidden=\"true\"></i>");
                pw.println("                                            <span class=\"rtl number\">&nbsp;&nbsp;"+searchBean.flight.getDate()+"</span>");
                pw.println("                                        </div>");
                pw.println("                                    </div>");
                pw.println("                                </div>");
                pw.println("                                <div class=\"col-md-6 rtl\">");
                pw.println("                                    <div class=\"row\">");
                pw.println("                                        <div class=\"col-md-12 rtl center in_white_row number\" id=\"in_white_middle\">");
                pw.println(c.convert_city(searchBean.flight.getOrig_code()) + " " + searchBean.flight.getDeparture_time() + " " + ">>" + " " + c.convert_city(searchBean.flight.getDest_code()) + " " + searchBean.flight.getArrival_time());
                pw.println("                                        </div>");
                pw.println("                                        <div class=\"col-md-4 rtl in_white_row center number\">");
                pw.println("                                            <i class=\"rtl t_font_awwsome fa fa-plane\" aria-hidden=\"true\"></i>");
                pw.println("                                            <span class=\"rtl number\">&nbsp;&nbsp;"+c.convert_airplane_model(searchBean.flight.getAirplane_model())+"</span>");
                pw.println("                                        </div>");
                pw.println("                                        <div class=\"col-md-8 rtl in_white_row center number\">");
                pw.println("                                             <i class=\"rtl t_font_awwsome fa fa-suitcase\" aria-hidden=\"true\"></i>");
                if (searchBean.seatClasses.get(i).getCount() < 9){
                    pw.println("                                            <span class=\"rtl\">&nbsp;&nbsp;" +searchBean.seatClasses.get(i).getCount()+ " صندلی باقیمانده " + c.convert_seat_class(searchBean.seatClasses.get(i).getSeat_class()) + "</span>");
                }
                else {
                    pw.println("                                            <span class=\"rtl\">&nbsp;&nbsp;" + c.convert_seat_class(searchBean.seatClasses.get(i).getSeat_class()) +"</span>");
                }

                pw.println("                                        </div>");
                pw.println("                                    </div>");
                pw.println("                                </div>");
                pw.println("                                <div class=\"col-md-3 rtl reserve\">");
                pw.println("                                    <div class=\"row\">");
                pw.println("                                        <div class=\"col-md-12 rtl center in_white_row number\">");
                pw.println( searchBean.total_prices.get(i) + "ریال" );
                pw.println("                                        </div>");
                pw.println("                                        <div class=\"col-md-12 rtl center in_white_row\">");
                pw.println("                                            <button type=\"submit\">رزرو آنلاین</button>");
                pw.println("                                        </div>");
                pw.println("                                    </div>");
                pw.println("                                </div>");
                pw.println("                            </div>");
                pw.println("                            </form>");
                pw.println("							<div class=\"row padding in_gray_row\">");
                pw.println("                                <div class=\"col-md-12 rtl titr\">");
                pw.println("                                </div> ");
                pw.println("                            </div>");

            }
        }
        pw.println("                        </div> ");
        //end
        pw.println("                    </div> ");
        pw.println("                    <div class=\"row\">");
        pw.println("                        <div class=\"rtl center col-md-12 number\" id=\"footer\">");
        pw.println("                            سپیده گودرزی ، مازیار نظری | دانشکده فنی دانشگاه تهران، بهار 1396");
        pw.println("                        </div>");
        pw.println("                    </div> ");
        pw.println("                </div>");
        pw.println("                <div  class=\"col-md-2\">");
        pw.println("                </div>");
        pw.println("            </div>");
        pw.println("        </div>");
        pw.print("        <script src=\"assets/js/Result.js\" type=\"text/javascript\"></script>");
        pw.println("    </body>");
        pw.println("</html>");

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            String from = request.getParameter("from");
            String to = request.getParameter("to");
            String depDate = request.getParameter("departureDate");
            String arrivalDate = request.getParameter("arrivalDate");
            String adultNum = request.getParameter("adultNum");
            String childNum = request.getParameter("childNum");
            String infNum = request.getParameter("infNum");
            logger.info("SRCH " + from + " " + to + " " + depDate);
            Akbarticket AT = new Akbarticket("188.166.78.119", 8081);
            System.out.println("[dbg] This is: "+ from + depDate + arrivalDate + to);
            VOSearchResponses searchResult= AT.search(from, to, depDate, adultNum, childNum, infNum);

            response.setContentType("text/html");
            response.setCharacterEncoding("utf-8");
            PrintWriter pw = response.getWriter();
            System.out.println("[dbg] before result page!!");
            result_page(pw, searchResult);

        }catch (Exception e){

        }

    }

    public void destroy(){


    }
}
