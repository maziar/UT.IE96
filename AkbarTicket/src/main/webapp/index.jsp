<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html>
    <head>
        <title>اکبر تیکت</title>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" type="text/css" href="../assets/css/Home-Search.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/cssreset.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <body>
        <div class="container">
            <img alt="BG" src="assets/Background.png" class="bgimage">
            <div class="header">
                <div id="company">
                    
                    <img alt="Logo" src="assets/LogoWhite.png" id="logo" >
                    <div class="inline company-name" >
                        اکبر تیکت <i class="fa fa-registered" id="icon-resize" aria-hidden="true"></i>
                    </div>
                    
                </div>
                <div class="user inline" >
                     <span id="username">&nbsp;نام کاربر&nbsp;</span><i class="fa fa-user" aria-hidden="true" id="user-icon"></i>
					<ul>
						<li>
							بلیطهای من
						</li>
						<li>
							خروج
						</li>
					</ul>
                </div>
            </div>
            <div class="ticket-kind">
                <div class="ticket1">
                    یک طرفه
                </div>
                <div class="ticket2" >
                    رفت و برگشت
                </div>
            </div>
            <form class="information" action="performSearch.do" method="POST">
                <div class="source" >
                    <i class="fa fa-plane" aria-hidden="true" id="plane"></i> &nbsp;مبدا 
                    <select class="src-select" name="from" >
                        <option value="THR">تهران - فرودگاه مهرآباد</option>
                        <option value="MHD">مشهد - فرودگاه مشهد</option>
                        <option value="IFN"> اصفهان - فرودگاه اصفهان</option>
                    </select>

                </div>
                <div class="dest">
                    <i class="fa fa-plane" aria-hidden="true" id="dest-plane"></i> &nbsp;مقصد 
                    <select class="dest-select" name="to">
                        <option value="THR">تهران - فرودگاه مهرآباد</option>
                        <option value="MHD">مشهد - فرودگاه مشهد</option>
                        <option value="IFN"> اصفهان - فرودگاه اصفهان</option>
                    </select>
                </div>
                <div class="go">
                    <i class="fa fa-calendar" aria-hidden="true" id="go-date"></i> &nbsp;رفت
                    <textarea class="date" name="departureDate"></textarea>

                </div>
                <div class="come">
                    <i class="fa fa-calendar" aria-hidden="true" id="come-date"></i> &nbsp;برگشت
                    <textarea class="enter-date" name="arrivalDate"></textarea>
                </div>
                <div>
                    <div class="adult-num-section" >
                        <div class="male-header">
                            بزرگسال
                        </div>
                        <i class="fa fa-male" aria-hidden="true" id="male-icon"></i>&nbsp;&nbsp;<select class="adult-num" name="adultNum">
                        <option class="FDigit" value="1">1</option>
                        <option class="FDigit" value="2">2</option>
                        <option class="FDigit" value="3">3</option>
                        </select>
                    </div>
                    <div class="child-num-section" >
                        <div class="child-header">
                            کودک زیر 12 سال
                        </div>
                        <i class="fa fa-child" aria-hidden="true" id="child-icon"></i>&nbsp;&nbsp;<select class="child-num" name="childNum">
                        <option class="FDigit" value="1">1</option>
                        <option class="FDigit" value="2">2</option>
                        <option class="FDigit" value="3">3</option>
                        </select>
                    </div>
                    <div class="inf-num-section" >
                        <div class="inf-header">
                            نوزاد زیر 2 سال
                        </div>
                        <i class="fa fa-child" aria-hidden="true"></i>&nbsp;&nbsp;<select class="inf-num" name="infNum">
                        <option class="FDigit" value="1">1</option>
                        <option class="FDigit" value="2">2</option>
                        <option class="FDigit" value="3">3</option>
                        </select>
                    </div>
                </div>
                <button class="search-button">جستجو</button>
            </form>
            <div class="css-slideshow"> 
                <figure> 
                    <img alt="1" src="assets/HomeSlideShow/1.png" />
                </figure> 
                <figure> 
                    <img alt="2" src="assets/HomeSlideShow/2.png" />
                </figure>
                <figure> 
                    <img alt="3" src="assets/HomeSlideShow/3.png" />
                </figure> 
                <figure> 
                    <img alt="4" src="assets/HomeSlideShow/4.png" />
                </figure> 
                <figure> 
                    <img alt="5" src="assets/HomeSlideShow/5.png" />
                </figure> 
                <figure> 
                    <img alt="6" src="assets/HomeSlideShow/6.png" />
                </figure> 
                <figure> 
                    <img alt="7" src="assets/HomeSlideShow/7.png" />
                </figure> 
                <figure> 
                    <img alt="8" src="assets/HomeSlideShow/8.png" />
                </figure> 
            </div>
            <div class="slogan" >
                سفر ارزان و آسان با<br /><br />اکبر تیکت
            </div>
            <div class="footer">
                مازیار نظری، سپیده گودرزی | دانشکده ی فنی دانشگاه تهران، بهار 1396
            </div>
        </div>

    </body>
</html>
